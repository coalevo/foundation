/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import net.coalevo.foundation.model.FoundationConstants;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.foundation.service.UUIDGeneratorService;
import net.coalevo.logging.model.LogProxy;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class is the <tt>BundleActivator</tt> implementation
 * for the coalevo foundation bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Marker c_LogMarker;
  private static LogProxy c_Log;

  private static ServiceMediator c_Services;
  private static Messages c_BundleMessages;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext) throws Exception {
    //1. Check start
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }

    //2. Start asynch
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //3. Foundation Services
              MessageResourceServiceImpl mrs = new MessageResourceServiceImpl();
              c_BundleMessages = mrs.getBundleMessages(bundleContext.getBundle());
              bundleContext.registerService(
                  MessageResourceService.class.getName(),
                  mrs,
                  null
              );
              log().info(c_LogMarker,
                  c_BundleMessages.get("Activator.activation.service", "service", "MessageResourceService"));

              log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "CoreService"));

              UUIDGeneratorService uidgens = new UUIDGeneratorServiceImpl();
              bundleContext.registerService(
                  UUIDGeneratorService.class.getName(),
                  uidgens,
                  null);
              log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "UUIDGeneratorService"));

              RndStringGeneratorService rndgens = new RndStringGeneratorServiceImpl();
              bundleContext.registerService(
                  RndStringGeneratorService.class.getName(),
                  rndgens,
                  null);
              log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "RndStringGeneratorService"));
            } catch (Exception ex) {
              log().error(c_LogMarker, "start(BundleContext)", ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    //wait start
    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if(c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  /**
   * Return the mediator for OSGi services.
   *
   * @return the {@link ServiceMediator}.
   */
  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  /**
   * Return the bundles logger.
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

  /**
   * Returns the bundles 18n messages.
   *
   * @return a {@link Messages} instance that provides i18n messages.
   */
  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  public static String getNodeIdentifier() {
    return System.getProperty(FoundationConstants.COALEVO_NODEID_KEY);
  }//getNodeIdentifier

}//class Activator
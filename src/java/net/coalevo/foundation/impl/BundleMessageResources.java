/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import net.coalevo.foundation.model.FoundationConstants;
import org.osgi.framework.Bundle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

/**
 * Provides message resources from bundle entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BundleMessageResources {

  private String m_BaseName;
  private Bundle m_Bundle;
  private Map<String, URL> m_Locales;

  public BundleMessageResources(Bundle b, String name) {
    m_Bundle = b;
    m_BaseName = name;
    resolve();
  }//BundleMessageResources

  public String getBaseName() {
    return m_BaseName;
  }//getBaseName


  private void resolve() {
    m_Locales = new HashMap<String, URL>();
    try {
      final boolean fromroot = (m_BaseName != null && m_BaseName.length() > 0 && m_BaseName.charAt(0) == '/');
      String path = FoundationConstants.MESSAGES_DIRECTORY;
      if (fromroot) {
        final int idx = m_BaseName.lastIndexOf("/");
        path = m_BaseName.substring(0, idx);
        m_BaseName = m_BaseName.substring(idx + 1, m_BaseName.length());
      }
      //find all available resources
      for (Enumeration e = m_Bundle.findEntries(path, m_BaseName + "*.xml", false); e != null && e.hasMoreElements();) {
        URL url = (URL) e.nextElement();
        //cut locales
        String file = url.getFile();
        file = file.substring(file.lastIndexOf(m_BaseName) + m_BaseName.length(), file.lastIndexOf(".xml"));
        //System.out.println(file+"::"+url.toString());
        m_Locales.put(file, url);
      }
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
    }
  }//resolve

  public InputStream getResourceAsStream(String suffix)
      throws IOException {
    return ((URL) m_Locales.get(suffix)).openStream();
  }//getResourceAsStream

  public void release() {
    m_Bundle = null;
    m_BaseName = null;
  }//release

  public List<String> getAvailableSuffixes(Locale l) {
    final List<String> suf = getLocaleSuffixes(l);
    for (int i = suf.size() - 1; i >= 0; i--) {
      String s = suf.get(i);
      if (!m_Locales.containsKey(s)) {
        suf.remove(i);
      }
    }
    return suf;
  }//getAvailableSuffixes

  /**
   * Prepares the possible keys for given locale.
   *
   * @param locale the locale
   * @return a <tt>List</tt> of keys.
   */
  public List<String> getLocaleSuffixes(Locale locale) {

    //Max we can have <language>_<country>_<variant>
    //insisting on a full path, these are three possible names
    final ArrayList<String> names = new ArrayList<String>(4);
    names.add("");
    //1. Language only
    final String lang = locale.getLanguage();
    if (lang.length() == 0) {
      return names;
    }
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(lang);
    names.add(sbuf.toString());

    //2. Language and country
    final String country = locale.getCountry();
    if (country.length() == 0) {
      return names;
    }
    sbuf.append('_');
    sbuf.append(country);
    names.add(sbuf.toString());

    //3. Language, country AND variant
    final String variant = locale.getVariant();
    if (variant.length() == 0) {
      return names;
    }
    sbuf.append('_');
    sbuf.append(variant);
    names.add(sbuf.toString());
    return names;
  }//getLocaleKeyPrefixes

}//class BundleMessageResources

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Partial (c) Tom Van Vleck
 * Please see http://www.multicians.org/thvv/gpw.html for history and info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

/**
 * Provides data for a pronounceable password generator.
 * <p/>
 * This code has been derived from the following source:
 * his program uses statistics on the frequency of three-letter sequences
 * in English to generate passwords.  The statistics are
 * generated from your dictionary by the program loadtris.
 * </p>
 * <p/>
 * See www.multicians.org/thvv/gpw.html for history and info.
 * Tom Van Vleck
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GpwData {

  static short c_Tris[][][] = new short[26][26][26];
  static long c_Sigma = 0; // 125729

  static {
    prepare();
  }//static initializer


  private static void set(int x1, int x2, int x3, short v) {
    c_Tris[x1][x2][x3] = v;
  }//set

  public static long get(int x1, int x2, int x3) {
    return (long) c_Tris[x1][x2][x3];
  }//get

  public static long getSigma() {
    return c_Sigma;
  }//getSigma


  private static void prepare() {
    int c1, c2, c3;
    for (c1 = 0; c1 < 13; c1++) {
      for (c2 = 0; c2 < 26; c2++) {
        for (c3 = 0; c3 < 26; c3++) {
          set(c1, c2, c3, TrisData1.TRIS[c1][c2][c3]);
        } // for c3
      } // for c2
    } // for c1
    for (c1 = 0; c1 < 13; c1++) {
      for (c2 = 0; c2 < 26; c2++) {
        for (c3 = 0; c3 < 26; c3++) {
          set(c1 + 13, c2, c3, TrisData2.TRIS[c1][c2][c3]);
        } // for c3
      } // for c2
    } // for c1
    for (c1 = 0; c1 < 26; c1++) {
      for (c2 = 0; c2 < 26; c2++) {
        for (c3 = 0; c3 < 26; c3++) {
          c_Sigma += (long) c_Tris[c1][c2][c3];
        } // for c3
      } // for c2
    } // for c1
  }//prepare

}//class GpwData

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;

/**
 * Class implementing a SAX handler for the templates defining
 * XML documents.
 * <p/>
 * This code is hand written, based on a simple state
 * machine. Note that it includes a whitespace handling
 * mechanism for convenience, that will allow the document
 * to be structured much better (as TAG indents won't
 * hurt any longer).
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageBundleHandler
    extends DefaultHandler {

  //state
  protected int m_State = STATE_UNDEFINED;
  protected Stack<String> m_Prefix;
  protected StringBuilder m_Buffer;
  protected MessagesImpl m_MessagesImpl;
  protected MessagesImpl.MessageTemplate m_MessageTemplate;
  protected MessagesImpl.MessageMap m_MessageMap;
  protected StringBuilder m_FormatBuffer;
  protected boolean m_FilterWhiteSpace = true;
  protected int m_Lines;
  protected boolean m_EndSpaceSwallowed = false;

  public MessageBundleHandler(MessagesImpl msg, MessagesImpl.MessageMap poolset) {
    m_MessageMap = poolset;
    m_Prefix = new Stack<String>();
    m_MessagesImpl = msg;
  }//constructor

  public void startElement(String uri, String name,
                           String qualifiedName, Attributes attributes)
      throws SAXException {

    try {
      switch (m_State) {
        case STATE_UNDEFINED:
          if (Tokens.MESSAGES.equals(name)) {
            m_State = STATE_MESSAGES;
          } else {
            throw new SAXException("Root element not found.");
          }
          break;
        case STATE_MESSAGES:
          if (Tokens.MESSAGESET.equals(name)) {
            handleMessageSet(attributes);
            m_State = STATE_MESSAGESET;
          } else if (Tokens.MESSAGE.equals(name)) {
            m_MessageTemplate = m_MessagesImpl.createTemplate();
            handleMessageAttributes(attributes);
            m_Buffer = new StringBuilder();
            m_State = STATE_MESSAGE;
          }
          break;
        case STATE_MESSAGESET:
          if (Tokens.MESSAGE.equals(name)) {
            m_MessageTemplate = m_MessagesImpl.createTemplate();
            handleMessageAttributes(attributes);
            m_Buffer = new StringBuilder();
            m_State = STATE_MESSAGE;
          } else if (Tokens.MESSAGESET.equals(name)) {
            handleMessageSet(attributes);
            m_State = STATE_MESSAGESET;
          }
          break;
      }
    } catch (Exception e) {
      throw new SAXException(e);
    }
  }//startElement

  public void characters(char[] chars, int start, int length)
      throws SAXException {

    switch (m_State) {
      case STATE_MESSAGE:
        m_EndSpaceSwallowed = filter(m_Buffer, chars, start, length);
        break;
    }
  }//characters

  private boolean filter(StringBuilder buf, char[] chars, int start, int length) {
    boolean result = false;
    //filter whitespace
    int end = start + length;
    for (int i = start; i < end; i++) {
      char aChar = chars[i];
      switch (aChar) {
        case 9:
        case 10:
        case 13:
        case 14:
          if (!m_FilterWhiteSpace) {
            buf.append(aChar);
          }
        case 32:
          if (m_FilterWhiteSpace) {
            //catch left edge space
            if (i == end - 1 && !(length == 1)) {
              result = true;
              continue;
            }
            int idx = i + 1;
            idx = ((idx > (chars.length - 1)) ? i : idx);
            if (chars[idx] != 32 && (!bufferEndsWithSpace())) {
              buf.append(aChar);
            }
          } else {
            buf.append(aChar);
          }
          break;
        default:
          buf.append(aChar);
          break;
      }
    }
    return result;
  }//filter

  public void ignorableWhitespace(char[] ch, int start, int length)
      throws SAXException {

  }//ignorableWhiteSpace

  public void endElement(String namespaceURI, String localName, String qName)
      throws SAXException {
    //log.debug("endElement()::" + m_State + "::" + localName);
    switch (m_State) {
      case STATE_MESSAGES:
        //parsing should be over

        break;
      case STATE_MESSAGESET:
        //decrease prefix
        if (!m_Prefix.isEmpty()) {
          m_Prefix.pop();
        }
        break;
      case STATE_MESSAGE:
        m_MessageTemplate.setContent(m_Buffer.toString());
        //add to database
        m_MessageMap.add(m_MessageTemplate);
        //turn on whitespace filter if off for template
        if (!m_FilterWhiteSpace) {
          m_FilterWhiteSpace = true;
        }
        //transition to template set
        m_State = STATE_MESSAGESET;
        break;
    }
  }//endElement

  private final void handleMessageSet(Attributes attr)
      throws SAXException {
    //1. id
    String str = attr.getValue("", Tokens.ID);
    if (str == null || str.length() == 0) {
      throw new SAXException("id missing.");
    } else if (Tokens.DEFAULT.equals(str)) {
      return;
    } else {
      String prefix = "";
      if (m_Prefix.isEmpty()) {
        prefix = new StringBuilder()
            .append(str)
            .append(Tokens.SETSEPARATOR)
            .toString();
      } else {
        prefix = new StringBuilder((String) m_Prefix.peek().toString())
            .append(str)
            .append(Tokens.SETSEPARATOR)
            .toString();
      }
      m_Prefix.push(prefix);
    }
  }//handleMessageSet

  private final void handleMessageAttributes(Attributes attr)
      throws SAXException {
    //1. id
    String str = attr.getValue("", Tokens.KEY);
    if (str == null || str.length() == 0) {
      throw new SAXException("key missing");
    } else {
      if (m_Prefix.isEmpty()) {
        m_MessageTemplate.setKey(str);
      } else {
        m_MessageTemplate.setKey((String) m_Prefix.peek() + str);
      }
    }
    //2. type
    str = attr.getValue("", Tokens.TYPE);
    if (str == null || str.length() == 0) {
      m_MessageTemplate.setStatic(true);
    } else if (Tokens.DYNAMIC.equals(str)) {
      m_MessageTemplate.setStatic(false);
    }
    str = attr.getValue("", Tokens.WSFILTER);
    if (str != null && str.length() > 0 && Tokens.OFF.equals(str)) {
      m_FilterWhiteSpace = false;
    }
  }//handleMessageAttributes

  private final boolean bufferEndsWithSpace() {
    return m_Buffer == null || m_Buffer.length() == 0 || (m_Buffer.charAt(m_Buffer.length() - 1) == 32);
  }//getLastBufferChar

  private static final int STATE_UNDEFINED = -1;
  private static final int STATE_MESSAGES = 1;
  private static final int STATE_MESSAGESET = 2;
  private static final int STATE_MESSAGE = 3;

  /**
   * Inner class defining the tokens to be encountered in
   * the document.
   */
  private final class Tokens {

    static final String MESSAGES = "messages";
    static final String MESSAGESET = "message-set";
    static final String MESSAGE = "message";
    static final String STATIC = "static";
    static final String DYNAMIC = "dynamic";
    static final String KEY = "key";
    static final String VALUE = "value";
    static final String TYPE = "type";
    static final String ID = "id";
    static final String DEFAULT = "default";
    static final char SETSEPARATOR = '.';
    static final String WSFILTER = "wsfilter";
    static final String OFF = "off";
    static final String LINES = "lines";
  }//inner class Tokens

}//class MessagesHandler

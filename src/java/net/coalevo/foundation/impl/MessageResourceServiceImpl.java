/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import net.coalevo.foundation.model.FoundationConstants;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.service.MessageResourceService;
import org.osgi.framework.Bundle;

/**
 * Provides an implementation of the {@link MessageResourceService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageResourceServiceImpl
    implements MessageResourceService {

  public Messages getBundleMessages(Bundle b) {
    return new MessagesImpl(new BundleMessageResources(b, FoundationConstants.MESSAGES_DIRECTORY + "messages"));
  }//getBundleMessages


  public Messages getBundleMessages(Bundle b, String basename) {
    return new MessagesImpl(new BundleMessageResources(b, basename));
  }//getBundleMessages

}//class MessageResourceServiceImpl

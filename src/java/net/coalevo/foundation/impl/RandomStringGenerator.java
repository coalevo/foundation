/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Partial (c) Tom Van Vleck
 * Please see http://www.multicians.org/thvv/gpw.html for history and info
 * about the generation of pronounceable passwords.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Provides a simple random string generator.
 * <p/>
 * The algorithm will generate a <tt>String</tt> assembled
 * from the following set of characters:<br/>
 * <tt>[a-z|A-Z|0-9]</tt>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
final class RandomStringGenerator {

  private static final Random c_Random;
  private static final char[] c_CharMap;

  static {
    c_Random = new SecureRandom();
    c_CharMap = new char[62];
    int idx = 0;
    for (int n = 48; n < 123; n++) {
      //jump to A
      if (n == 58) {
        n = 65;
      } else if (n == 91) {
        n = 97;
      }
      c_CharMap[idx++] = (char) n;
    }
  }//static initializer

  /**
   * Convenience method that returns a random generated
   * password of length 8.
   *
   * @return a random generated password string.
   */
  public static String getString() {
    return getString(8);
  }//getString

  /**
   * Returns a random generated password of given length.
   *
   * @param len the length of the password to be generated.
   * @return the random generated password.
   */
  public static String getString(int len) {
    final StringBuilder sbuf = new StringBuilder();
    for (int i = 0; i < len; i++) {
      char ch = c_CharMap[c_Random.nextInt(62)];
      sbuf.append(ch);
    }
    return sbuf.toString();
  }//getString

  public static String getPronounceableString() {
    return generatePronounceAble(8);
  }//getPronounceableString

  public static String getPronounceableString(int len) {
    return generatePronounceAble(len);
  }//getPronounceableString


  private static String generatePronounceAble(int pwl) {
    int c1, c2, c3;
    long sum = 0;
    int nchar;
    long ranno;
    double pik;
    StringBuilder password;

    password = new StringBuilder(pwl);
    pik = c_Random.nextDouble(); // random number [0,1]
    ranno = (long) (pik * GpwData.getSigma()); // weight by sum of frequencies
    sum = 0;
    for (c1 = 0; c1 < 26; c1++) {
      for (c2 = 0; c2 < 26; c2++) {
        for (c3 = 0; c3 < 26; c3++) {
          sum += GpwData.get(c1, c2, c3);
          if (sum > ranno) {
            password.append(alphabet.charAt(c1));
            password.append(alphabet.charAt(c2));
            password.append(alphabet.charAt(c3));
            c1 = 26; // Found start. Break all 3 loops.
            c2 = 26;
            c3 = 26;
          } // if sum
        } // for c3
      } // for c2
    } // for c1

    // Now do a random walk.
    nchar = 3;
    while (nchar < pwl) {
      c1 = alphabet.indexOf(password.charAt(nchar - 2));
      c2 = alphabet.indexOf(password.charAt(nchar - 1));
      sum = 0;
      for (c3 = 0; c3 < 26; c3++)
        sum += GpwData.get(c1, c2, c3);
      if (sum == 0) {
        break;  // exit while loop
      }
      pik = c_Random.nextDouble();
      ranno = (long) (pik * sum);
      sum = 0;
      for (c3 = 0; c3 < 26; c3++) {
        sum += GpwData.get(c1, c2, c3);
        if (sum > ranno) {
          password.append(alphabet.charAt(c3));
          c3 = 26; // break for loop
        } // if sum
      } // for c3
      nchar++;
    }
    return password.toString();
  }//generatePronounceable

  public static void main(String[] args) {
    for (int i = 0; i < 200; i++) {
      System.out.println(RandomStringGenerator.getString());
    }
  }//main

  private final static String alphabet = "abcdefghijklmnopqrstuvwxyz";

}//class RandomStringGenerator

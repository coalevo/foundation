/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import org.osgi.framework.*;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import javax.xml.parsers.SAXParserFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Implements a mediator pattern class for services from the OSGi container.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());

  private BundleContext m_BundleContext;

  private SAXParserFactory m_SAXParserFactory;

  private CountDownLatch m_SAXParserFactoryLatch;

  public SAXParserFactory getSAXParserFactory(long wait) {
    try {
      if (wait < 0) {
        m_SAXParserFactoryLatch.await();
      } else if (wait > 0) {
        m_SAXParserFactoryLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getSAXParserFactory()", e);
    }

    return m_SAXParserFactory;
  }//getSAXParserFactory

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;

    m_SAXParserFactoryLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(objectclass=" + SAXParserFactory.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    m_SAXParserFactory = null;

    if(m_SAXParserFactoryLatch != null) {
      m_SAXParserFactoryLatch.countDown();
      m_SAXParserFactoryLatch = null;
    }

    m_BundleContext = null;
    m_LogMarker = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          }else if (o instanceof SAXParserFactory) {
            m_SAXParserFactory = (SAXParserFactory) o;
            m_SAXParserFactory.setValidating(false);
            m_SAXParserFactory.setNamespaceAware(true);
            m_SAXParserFactoryLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SAXParserFactory) {
            m_SAXParserFactory = null;
            m_SAXParserFactoryLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.impl;

import net.coalevo.foundation.service.UUIDGeneratorService;
import net.coalevo.foundation.util.crypto.UUIDGenerator;
import org.apache.commons.codec.binary.Base64;


/**
 * Provides an implementation of {@link UUIDGeneratorService}
 * that uses {@link UUIDGenerator}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UUIDGeneratorServiceImpl
    implements UUIDGeneratorService {

  public final String getUID() {
    return UUIDGenerator.getUID();
  }//getUID

  public final byte[] getRawUID() {
    return UUIDGenerator.getRawUID();
  }//getRawUID

  public String toUID(byte[] ruid) {
    return new String(Base64.encodeBase64(ruid));
  }//translateRawUID

  public byte[] toRawUID(String uid) {
    return Base64.decodeBase64(uid.getBytes());
  }//translateRawUID

}//class UUIDGeneratorServiceImpl

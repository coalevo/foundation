/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Implements an <tt>Iterator</tt> over
 * all actions representing {@link Action} instances
 * of a {@link net.coalevo.foundation.model.Service}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ActionIterator
    implements Iterator<Action> {

  private int m_Cursor;
  private final Action[] m_Actions;

  public ActionIterator() {
    m_Actions = EMPTY;
  }//constructor

  public ActionIterator(Action[] actions) {
    m_Actions = ((actions == null) ? EMPTY : actions);
  }//ActionIterator

  public boolean hasNext() {
    return m_Cursor < m_Actions.length;
  }//hasNext

  public Action next() {
    if (!(m_Cursor < m_Actions.length)) {
      throw new NoSuchElementException();
    }
    return m_Actions[m_Cursor++];
  }//next

  public void remove() {
    throw new UnsupportedOperationException();
  }//remove

  public static final ActionIterator EMPTY_ITERATOR = new ActionIterator();
  private static final Action[] EMPTY = new Action[0];


}//class ActionIterator

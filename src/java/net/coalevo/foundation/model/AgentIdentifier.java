/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import java.io.Serializable;

/**
 * Provides a proxy identifier for an {@link Agent},
 * which identifies but cannot impersonate.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AgentIdentifier
    implements Identifiable, Serializable {

  static final long serialVersionUID = -2844638901302595094L;

  private static final String c_LocalNode;
  private final String m_Identifier;
  private final String m_Name;
  private final String m_Node;
  private final boolean m_Local;

  static {
    c_LocalNode = System.getProperty(FoundationConstants.COALEVO_NODEID_KEY);
  }//static

  public AgentIdentifier(String id) {
    String[] split = id.split("@");
    if (split == null || split.length == 1) {
      m_Name = id;
      m_Node = c_LocalNode;
      m_Identifier = m_Name + "@" + m_Node;
      m_Local = true;
    } else if (split.length == 2) {
      m_Name = split[0];
      m_Node = split[1];
      m_Identifier = id;
      m_Local = m_Node.equals(c_LocalNode);
    } else {
      throw new IllegalArgumentException();
    }
  }//AgentIdentifier

  /**
   * Tests if this <tt>AgentIdentifier</tt> is local.
   * <p/>
   * This method will return true if the node part of
   * the identifier equals the actual node identifier.
   * </p>
   *
   * @return true if local, false otherwise.
   */
  public boolean isLocal() {
    return m_Local;
  }//isLocal

  /**
   * Returns this <tt>AgentIdentifier</tt> as string.
   *
   * @return the <tt>AgentIdentifier</tt> as string.
   */
  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  /**
   * Returns the name part of this <tt>AgentIdentifier</tt>.
   *
   * @return the name part of this identifier as string.
   */
  public String getName() {
    return m_Name;
  }//getName

  /**
   * Returns the node part of this <tt>AgentIdentifier</tt>.
   *
   * @return the node part of this identifier as string.
   */
  public String getNode() {
    return m_Node;
  }//getNode

  /**
   * Returns {@link #getIdentifier()}.
   *
   * @return the <tt>AgentIdentifier</tt> as string.
   */
  public String toString() {
    if (isLocal()) {
      return m_Name;
    } else {
      return m_Identifier;
    }
  }//toString

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final AgentIdentifier that = (AgentIdentifier) o;

    if (m_Identifier != null ? !m_Identifier.equals(that.m_Identifier) : that.m_Identifier != null) return false;

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

  /**
   public static void main(String[] args) {
   AgentIdentifier aid = new AgentIdentifier("root");
   System.out.println(aid.toString());
   System.out.println(aid.getName());
   System.out.println(aid.getNode());
   System.out.println(aid.isLocal());

   aid = new AgentIdentifier("dieter@wimpi.net");
   System.out.println(aid.toString());
   System.out.println(aid.getName());
   System.out.println(aid.getNode());
   System.out.println(aid.isLocal());
   }//main
   **/

}//class AgentIdentifier

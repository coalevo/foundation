/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import net.coalevo.foundation.util.LRUCacheMap;

/**
 * Provides an LRU cache for {@link AgentIdentifier} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AgentIdentifierInstanceCache {

  private LRUCacheMap<String, AgentIdentifier> m_Cache;

  /**
   * Constructs a new cache with size of 50.
   */
  public AgentIdentifierInstanceCache() {
    this(50);
  }//constructor

  /**
   * Constructs a new cache with a given size.
   *
   * @param size the size as <tt>int</tt>.
   */
  public AgentIdentifierInstanceCache(int size) {
    m_Cache = new LRUCacheMap<String, AgentIdentifier>(size);
  }//constructor(int)

  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  /**
   * Sets maximum number of cached instances.
   *
   * @param size the maximum cached instances.
   */
  public void setCeiling(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
    }
  }//setCacheSize

  public AgentIdentifier get(String id) {
    Object o = null;
    synchronized (m_Cache) {
      o = m_Cache.get(id);
    }
    if (o != null) {
      return (AgentIdentifier) o;
    } else {
      AgentIdentifier aid = new AgentIdentifier(id);
      synchronized (m_Cache) {
        m_Cache.put(id, aid);
      }
      return aid;
    }
  }//getAgentIdentifier

  public void put(AgentIdentifier aid) {
    synchronized (m_Cache) {
      m_Cache.put(aid.getIdentifier(), aid);
    }
  }//put

  public String asString() {
    return m_Cache.toString();
  }//toString

  public void clear() {
    m_Cache.clear();
  }//clear

}//class AgentIdentifierInstanceCache

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

/**
 * Provides an abstract base {@link Identifiable}.
 * <p/>
 * The identifying <tt>String</tt> is handled as
 * <em>final</em>.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseIdentifiable implements Identifiable {

  protected final String m_Identifier;

  public BaseIdentifiable(String id) {
    m_Identifier = id;
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public String toString() {
    return m_Identifier;
  }//toString

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Identifiable)) return false;

    final BaseIdentifiable baseIdentifiable = (BaseIdentifiable) o;

    if (m_Identifier != null ? !m_Identifier.equals(baseIdentifiable.m_Identifier) : baseIdentifiable.m_Identifier != null)
      return false;

    return true;
  }//equals

}//class BaseIdentifiable

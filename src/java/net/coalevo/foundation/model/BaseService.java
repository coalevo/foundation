/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import java.util.Iterator;

/**
 * Provides a base implementation stub for {@link Service}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseService
    extends BaseIdentifiable
    implements Service {

  protected final Action[] m_Actions;
  private final AgentIdentifier m_AgentIdentifier;

  public BaseService(String id, Action[] actions) {
    super(id);
    m_AgentIdentifier = new AgentIdentifier(id);
    m_Actions = actions;
  }//BaseService

  public Iterator<Action> getActions() {
    return new ActionIterator(m_Actions);
  }//getActions

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

}//class BaseService

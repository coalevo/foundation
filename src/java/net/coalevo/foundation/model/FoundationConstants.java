/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

/**
 * Defines constants for the Coalevo foundation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface FoundationConstants {

  /**
   * Defines the directory within a Bundle that contains the
   * internationalized message resource files.
   * <p/>
   * This is defined to be <tt>/OSGI-INF/l10n</tt> the bundles
   * localization directory as defined per R4 specification.
   * </p>
   */
  public final static String MESSAGES_DIRECTORY = "/OSGI-INF/l10n/";

  /**
   * Defines the system property name that should hold the coalevo
   * data directory.
   * <p/>
   * This value is defined to be <tt>net.coalevo.datadir</tt>
   * </p>
   */
  public final static String COALEVO_DATADIR_KEY = "net.coalevo.datadir";

  /**
   * Defines the system property name that should hold the coalevo
   * node identifier.
   * <p/>
   * This value is defined to be <tt>net.coalevo.nodeid</tt>
   * </p>
   */
  public final static String COALEVO_NODEID_KEY = "net.coalevo.nodeid";

}//interface FoundationConstants

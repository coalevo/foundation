/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

/**
 * This exception is thrown if an error occurs during a
 * maintenance operation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MaintenanceException
    extends Exception {

  /**
   * Constructs a new <tt>MaintenanceException</tt>.
   */
  public MaintenanceException() {
  }//constructor

  /**
   * Constructs a new <tt>MaintenanceException</tt> with a
   * given message.
   *
   * @param message the message as <tt>String</tt>.
   */
  public MaintenanceException(String message) {
    super(message);
  }//constructor(String)

  /**
   * Constructs a new <tt>MaintenanceException</tt> with
   * a given message and cause.
   *
   * @param message the message as <tt>String</tt>.
   * @param cause   a <tt>Throwable</tt>.
   */
  public MaintenanceException(String message, Throwable cause) {
    super(message, cause);
  }//constructor(String,Throwable)

}//class MaintenanceException

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

/**
 * Interface defining the contract for
 * message attribute instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessageAttributes {

  /**
   * Adds an attribute to this <tt>MessageAttributes</tt>.
   * Note that duplicate name references will automatically
   * be translated into a multivalue attribute (iterateable),
   * in a guaranteed FIFO sequence.
   *
   * @param name  the name of the attribute.
   * @param value the value.
   */
  public void add(String name, String value);

  /**
   * Adds an aggregate attribute to this <tt>MessageAttributes</tt>.
   * Notes:
   * <ul>
   * <li>Aggregates require a composite key value e.g.<tt>names.{first,last}</tt>.</li>
   * <li>Each duplicate key will be translated automatically into a
   * multivalue attribute (iterateable), in a guaranteed FIFO sequence.</li>
   * <li>A maximum of 5 aggregated values can be handled at the moment.</li>
   * </ul>
   * <p/>
   *
   * @param aggrname the name of the aggregate value.
   * @param values   the values (max 5).
   */
  public void add(String aggrname, String[] values);

}//interface MessageAttributes

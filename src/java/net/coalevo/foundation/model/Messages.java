/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import java.util.Locale;

/**
 * Provides the interface for obtaining i18n messages.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Messages {

  /**
   * Returns the message with the given key and
   * in the default <tt>Locale</tt>.
   *
   * @param key the message key.
   * @return the message string.
   */
  public String get(String key);

  /**
   * Returns the message with the given key, attributes
   * the default <tt>Locale</tt>.
   * <p/>
   * Attributes should be leased from this instance
   * using {@link #leaseAttributes}. The will be
   * automatically recycled after a call to this method,
   * so your application should not use the reference
   * any further.
   * </p>
   *
   * @param key  the message key.
   * @param attr the attributes for the message.
   * @return the formatted message string.
   * @see #leaseAttributes
   */
  public String get(String key, MessageAttributes attr);

  /**
   * Convenience method that returns the message with the given key
   * and the specified attribute in the default <tt>Locale</tt>.
   *
   * @param key  the message key.
   * @param name an attribute name.
   * @param val  attribute value.
   * @return the formatted message string.
   */
  public String get(String key, String name, String val);

  /**
   * Convenience method that returns the message with the given key
   * and the specified attributes in the default <tt>Locale</tt>.
   *
   * @param key   the message key.
   * @param name1 an attribute name.
   * @param val1  attribute value.
   * @param name2 an attribute name.
   * @param val2  attribute value.
   * @return the formatted message string.
   */
  public String get(String key, String name1, String val1, String name2, String val2);

  /**
   * Returns the message with the given key and
   * if available <tt>Locale</tt>.
   *
   * @param l   the locale.
   * @param key the message key.
   * @return the message string.
   */
  public String get(Locale l, String key);

  /**
   * Returns the message with the given key, attributes
   * and <tt>Locale</tt>.
   * <p/>
   * Attributes should be leased from this instance
   * using {@link #leaseAttributes}. The will be
   * automatically recycled after a call to this method,
   * so your application should not use the reference
   * any further.
   * </p>
   *
   * @param l    the locale.
   * @param key  the message key.
   * @param attr the attributes for the message.
   * @return the formatted message string.
   * @see #leaseAttributes
   */
  public String get(Locale l, String key, MessageAttributes attr);


  /**
   * Convenience method that returns the message with the given key
   * and the specified attribute in the default <tt>Locale</tt>.
   *
   * @param l    the locale.
   * @param key  the message key.
   * @param name an attribute name.
   * @param val  attribute value.
   * @return the formatted message string.
   */
  public String get(Locale l, String key, String name, String val);

  /**
   * Convenience method that returns the message with the given key
   * and the specified attributes in the default <tt>Locale</tt>.
   *
   * @param l     the locale.
   * @param key   the message key.
   * @param name1 an attribute name.
   * @param val1  attribute value.
   * @param name2 an attribute name.
   * @param val2  attribute value.
   * @return the formatted message string.
   */
  public String get(Locale l, String key, String name1, String val1, String name2, String val2);

  /**
   * Returns a leased {@link MessageAttributes} instance.
   * This instance should be used immediately and passed to
   * {@link #get(Locale,String,MessageAttributes)}, which will
   * recycle the instance returning it to the originating pool.
   *
   * @return a {@link MessageAttributes}  instance.
   */
  public MessageAttributes leaseAttributes();

}//interface Messages

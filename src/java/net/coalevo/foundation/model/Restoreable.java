/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import java.io.File;

/**
 * Defines the contract for a <tt>Restoreable</tt>
 * instance.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Restoreable {

  /**
   * Executes the backup routine of this <tt>Restoreable</tt>.
   * <p/>
   * The <tt>File</tt> should represent the base directory of
   * a complete system backup. Each <tt>Restoreable</tt> should
   * either create a single uniquely named file or directory
   * for storing its backup information.
   * </p>
   * <p/>
   * The file or directory can be tagged, this should allow
   * to establish a system backup schema with a timedate for example.
   * </p>
   *
   * @param caller an {@link Agent} instance.
   * @param f      a <tt>File</tt> instance representing a base directory.
   * @param tag    a <tt>String</tt>.
   * @throws SecurityException if the calling {@link Agent}
   *                           is not authorized or authentic.
   */
  public void doBackup(Agent caller, File f, String tag)
      throws SecurityException, BackupException;

  /**
   * Executes the restore routine of this <tt>Restoreable</tt>.
   * <p/>
   * The <tt>File</tt> should represent the base directory of
   * a backup.
   * </p>
   *
   * @param f   a <tt>File</tt> instance representing a base directory.
   * @param tag a <tt>String</tt>.
   * @throws SecurityException if the calling {@link Agent}
   *                           is not authorized or authentic.
   */
  public void doRestore(Agent caller, File f, String tag)
      throws SecurityException, RestoreException;

  /**
   * Defines the {@link Action} for {@link #doBackup}.
   */
  public static Action DO_BACKUP = new Action("doBackup");

  /**
   * Defines the {@link Action} for {@link #doRestore}.
   */
  public static Action DO_RESTORE = new Action("doRestore");


}//interface Restoreable

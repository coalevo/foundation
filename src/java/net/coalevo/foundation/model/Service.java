/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import org.osgi.framework.BundleContext;

import java.util.Iterator;

/**
 * This interface defines the basic contract for
 * a <tt>Service</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Service
    extends Identifiable {

  /**
   * Activates this <tt>Service</tt>.
   *
   * @return true if activated, false otherwise.
   */
  public boolean activate(BundleContext bc);

  /**
   * Deactivates this <tt>Service</tt>.
   *
   * @return tru if deactivated, false otherwise.
   */
  public boolean deactivate();

  /**
   * Returns an <tt>Iterator</tt> over the actions
   * provided by this <tt>Service</tt>.
   *
   * @return an <tt>Iterator</tt>.
   */
  public Iterator<Action> getActions();

}//interface Service

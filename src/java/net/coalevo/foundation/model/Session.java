/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.model;

import java.util.Iterator;

/**
 * Provides a way to establish a session within the Coalevo platform,
 * between some active entity and the platform  (which is basically a
 * composition of a number of services).
 * <p/>
 * A <tt>Session</tt> represents a binding that gives a shared context to
 * a sequence of interactions with available services. The most important
 * aspect of this context is related to security. The associated
 * {@link Agent} that has been authenticated and provides authorizations
 * for exerting privileged actions.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Session {

  /**
   * Returns a string containing the unique identifier
   * assigned to this <tt>Session</tt>.
   *
   * @return a string specifying the identifier assigned to this <tt>Session</tt>.
   */
  public String getIdentifier();

  /**
   * Returns the {@link Agent} instance bound to this
   * <tt>Session</tt>.
   *
   * @return the bound {@link Agent} instance.
   * @throws IllegalStateException if this method is called on an
   *                               invalidated or new session.
   */
  public UserAgent getAgent() throws IllegalStateException;

  /**
   * Returns the time when this <tt>Session</tt> was created,
   * measured in milliseconds since midnight January 1, 1970 GMT.
   *
   * @return a <tt>long</tt> specifying when this session was created, expressed in
   *         milliseconds since midnight January 1, 1970 GMT.
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          invalidated session.
   */
  public long getCreationTime() throws IllegalStateException;

  /**
   * Returns the last time the client sent a request associated with this
   * <tt>Session</tt>, as the number of milliseconds since midnight
   * January 1, 1970 GMT.
   * <p/>
   * Getting or setting an attribute associated with this
   * <tt>Session</tt>, do not affect the access time.
   * </p>
   *
   * @return a long representing the last time a service handled a request
   *         associated with this <tt>Session</tt>, expressed in milliseconds
   *         since midnight January 1, 1970 GMT.
   */
  public long getLastAccessedTime();

  /**
   * Returns the maximum idle time allowed for this <tt>Session</tt>,
   * measured in milliseconds since midnight January 1, 1970 GMT.
   *
   * @return a <tt>long</tt> specifying maximum idle time, expressed in
   *         milliseconds since midnight January 1, 1970 GMT.
   */
  public long getMaxIdleTime();

  /**
   * Invalidates this <tt>Session</tt> and unbinds
   * any attributes bound to it.
   *
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          already invalidated session.
   */
  public void invalidate();

  /**
   * Tests if this <tt>Session</tt> is valid.
   *
   * @return true if valid, false otherwise.
   */
  public boolean isValid();

  /**
   * Mark an access to this session, updating
   * the idle time.
   *
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          already invalidated session.
   */
  public void access() throws IllegalStateException;

  /**
   * Returns the object bound with the specified name in this
   * <tt>Session</tt>, or null if no object is bound under the name.
   *
   * @param name a <tt>String</tt> specifying a name for an object.
   * @return the <tt>Object</tt> bound to the specified name.
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          invalidated session.*
   */
  public Object getAttribute(String name) throws IllegalStateException;

  /**
   * Binds an <tt>Object</tt> to this <tt>Session</tt>, using the name
   * specified. If an object of the same name is already bound to the
   * session, the object is replaced.
   *
   * @param name  a <tt>String</tt> specifying a name for an object.
   * @param value the <tt>Object</tt> to be bound to the specified name.
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          invalidated session.
   */
  public void setAttribute(String name, Object value) throws IllegalStateException;

  /**
   * Removes the <tt>Object</tt> bound with the specified name from this
   * <tt>Session</tt>.
   * If the session does not have an object bound with the specified name,
   * this method does nothing.
   *
   * @param name a <tt>String</tt> specifying a name for an object.
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          invalidated session.
   */
  public void removeAttribute(String name) throws IllegalStateException;

  /**
   * Returns an <tt>Iterator</tt> of <tt>String</tt> objects containing the names
   * of all the objects bound to this <tt>Session</tt>.
   *
   * @return an <tt>Iterator</tt> of <tt>String</tt> objects containing the names
   *         of all the objects bound to this <tt>Session</tt>.
   * @throws java.lang.IllegalStateException
   *          if this method is called on an
   *          invalidated session.
   */
  public Iterator<String> getAttributeNames() throws IllegalStateException;

  /**
   * Tests if this <tt>Session</tt> has a specific named attribute set.
   *
   * @param name a <tt>String</tt> specifying a name for an object.
   * @return true if set, false otherwise.
   * @throws IllegalStateException if this method is called on an
   *                               invalidated session.
   */
  public boolean hasAttribute(String name) throws IllegalStateException;

  /**
   * Adds a {@link SessionListener} for this <tt>Session</tt>.
   *
   * @param l a {@link SessionListener} instance.
   */
  public void addSessionListener(SessionListener l);

  /**
   * Removes a {@link SessionListener} from this <tt>Session</tt>.
   *
   * @param l a {@link SessionListener} instance.
   */
  public void removeSessionListener(SessionListener l);

}//interface Session

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.service;

import net.coalevo.foundation.model.Messages;
import org.osgi.framework.Bundle;

/**
 * Provides a service for the creation of i18n
 * message resources from bundle owned XML resource files.
 * <p/>
 * Note that this service should provide a bundle it's own messages,
 * and that only the owning bundle should load it's own resources, so
 * that when a bundle is updated, messages are automatically reloaded.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessageResourceService {

  /**
   * Returns the bundle messages for the specified bundle.
   *
   * @param b the <tt>Bundle</tt> for which the messages are requested.
   * @return a {@link Messages} instance.
   */
  public Messages getBundleMessages(Bundle b);

  /**
   * Returns the bundle messages for the specified bundle with the
   * given basename.
   *
   * @param b        the <tt>Bundle</tt> for which the messages are requested.
   * @param basename the basename of the resources.
   * @return a {@link Messages} instance.
   */
  public Messages getBundleMessages(Bundle b, String basename);

}//interface MessageResourceService

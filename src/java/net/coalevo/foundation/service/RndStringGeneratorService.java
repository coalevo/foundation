/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.service;

/**
 * Provides a {@link net.coalevo.foundation.model.Service} for
 * obtaining alphanumeric random strings.
 * <p/>
 * The string might be useful as password or validation key
 * for example.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface RndStringGeneratorService {

  /**
   * Returns a random generated <tt>String</tt> of a given length.
   * <p/>
   *
   * @param length the length of the string to be generated.
   * @return a random generated <tt>String</tt>.
   */
  public String getRandomString(int length);

  /**
   * Returns a random generated <tt>String</tt> of length 8.
   * <p/>
   *
   * @return a random generated <tt>String</tt>.
   */
  public String getRandomString();

  /**
   * Returns a random generated pronounceable
   * <tt>String</tt> of a given length.
   * <p/>
   *
   * @param length the length of the string to be generated.
   * @return a random generated <tt>String</tt>.
   */
  public String getPronounceableString(int length);

  /**
   * Returns a random generated pronounceable <tt>String</tt>
   * of length 8.
   * <p/>
   *
   * @return a random generated <tt>String</tt>.
   */
  public String getPronounceableString();

}//interface RndStringGeneratorService
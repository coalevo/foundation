/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.service;

/**
 * Provides a {@link net.coalevo.foundation.model.Service} for
 * obtaining alphanumeric unique identifiers.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface UUIDGeneratorService {

  /**
   * Returns a UID (unique identifier) as <tt>String</tt>.
   * <p/>
   * The identifier represents the MD5 hashed combination
   * of a completely random padding, a temporal (system time)
   * and a spatial (IP address) component, transformed to a
   * hex String.
   *
   * @return a UID as <tt>String</tt>.
   */
  public String getUID();

  /**
   * Returns a UID (unique identifier) as <tt>byte[]</tt>.
   * The identifier represents the MD5 hashed combination
   * of a completely random padding, a temporal (system time)
   * and a spatial (IP address) component.
   *
   * @return a raw UID as <tt>byte[]</tt>.
   */
  public byte[] getRawUID();

  /**
   * Translates a raw identifier into a hexadecimal
   * <tt>String</tt> representation.
   *
   * @param ruid the identifier to be translated as <tt>byte[]</tt>.
   * @return the unique identifier as <tt>String</tt>.
   */
  public String toUID(byte[] ruid);

  /**
   * Translates a raw identifier into a hexadecimal
   * <tt>String</tt> representation.
   *
   * @param uid the identifier to be translated as <tt>String</tt>.
   * @return the unique identifier as raw <tt>byte[16]</tt>.
   */
  public byte[] toRawUID(String uid);

}//interface UUIDGeneratorService
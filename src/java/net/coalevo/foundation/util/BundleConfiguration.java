/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util;

import net.coalevo.foundation.impl.Activator;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

import java.util.Dictionary;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * Provides a base implementation for a unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BundleConfiguration
    extends BaseService
    implements ManagedService {

  private static Logger log = LoggerFactory.getLogger(BundleConfiguration.class);
  private BundleContext m_BundleContext;
  private CountDownLatch m_Latch;
  private ConfigurationMediator m_ConfigurationMediator;

  public BundleConfiguration(String identifier) {
    super(identifier, null);
    m_ConfigurationMediator = new ConfigurationMediator();
  }//constructor

  /**
   * Returns the associated {@link ConfigurationMediator}.
   *
   * @return a {@link ConfigurationMediator} instance.
   */
  public ConfigurationMediator getConfigurationMediator() {
    return m_ConfigurationMediator;
  }//getConfigurationMediator

  public void updated(Dictionary conf)
      throws ConfigurationException {
    log.debug("updated(Dictionary)" + ((conf == null) ? "NULL" : conf.toString()));
    try {
      //Note: if the www is null, then the dictionary will automatically return
      //defaults for all values.
      final MetaTypeDictionary config = new MetaTypeDictionary(conf, m_BundleContext, getIdentifier());
      m_ConfigurationMediator.update(config);
    } catch (UnsupportedOperationException ex) {
      log.error("updated()", ex);
      throw new ConfigurationException("", Activator.getBundleMessages().get("BundleConfiguration.exception.nometatype"), ex);
    } finally {
      if (conf != null) {
        m_Latch.countDown();
      }
    }
  }//updated

  public boolean activate(BundleContext bc) {
    m_BundleContext = bc;
    //register the service
    m_Latch = new CountDownLatch(1);
    final Properties p = new Properties();
    p.put(org.osgi.framework.Constants.SERVICE_PID, getIdentifier());
    String[] classes = new String[]{ManagedService.class.getName()};
    m_BundleContext.registerService(classes, this, p);
    //config admin
    ConfigurationAdmin ca = null;
    ServiceReference ref = m_BundleContext.getServiceReference(
        ConfigurationAdmin.class.getName());
    if (ref != null) {
      ca = (ConfigurationAdmin) m_BundleContext.getService(ref);
    }
    if (ca == null) {
      log.error(Activator.getBundleMessages().get("BundleConfiguration.exception.noconfigadmin"));
      return false;
    }
    try {
      Configuration[] c = ca.listConfigurations("(" + org.osgi.framework.Constants.SERVICE_PID + "=" + getIdentifier() + ")");
      if (c == null || c.length == 0) {
        Configuration config = ca.getConfiguration(getIdentifier());
        //update with defaults
        config.update(new MetaTypeDictionary(null, m_BundleContext, getIdentifier()).getDictionary());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      log.error("activate()", ex);
      return false;
    }

    try {
      m_Latch.await();
    } catch (InterruptedException iex) {
      log.error("activate()", iex);
      return false;
    }
    return true;
  }//activate

  public boolean deactivate() {
    m_ConfigurationMediator.clearHandlers();
    m_Latch = null;
    m_BundleContext = null;
    m_ConfigurationMediator = null;
    return true;
  }//deactivate

}//class BundleConfiguration

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util;

import net.coalevo.foundation.model.MessageAttributes;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.service.MessageResourceService;

import java.util.HashMap;
import java.util.Locale;

/**
 * Implements {@link Messages} without actual message resolving.
 * <p/>
 * This is an utility class that can be instantiated and used
 * for debugging, or when the {@link MessageResourceService} is not
 * available.
 *
 * @author Dieter Wimberger
 */
public class DummyMessages
    implements Messages {

  public String get(String key) {
    return key;
  }//get

  public String get(String key, MessageAttributes attr) {
    return key + attr.toString();
  }//get

  public String get(String key, String name, String val) {
    return key + ":" + name + "=" + val;
  }//get

  public String get(String key, String name1, String val1, String name2, String val2) {
    return key + ":" + name1 + "=" + val1 + name2 + "=" + val2;
  }//get

  public String get(Locale l, String key) {
    return key;
  }//get

  public String get(Locale l, String key, MessageAttributes attr) {
    return key + attr.toString();
  }//get

  public String get(Locale l, String key, String name, String val) {
    return key + ":" + name + "=" + val;
  }//get

  public String get(Locale l, String key, String name1, String val1, String name2, String val2) {
    return key + ":" + name1 + "=" + val1 + name2 + "=" + val2;
  }//get

  public MessageAttributes leaseAttributes() {
    return new DummyAttributes();
  }//get

  private class DummyAttributes
      extends HashMap
      implements MessageAttributes {

    public void add(String name, String value) {
      put(name, value);
    }//add

    public void add(String aggrname, String[] values) {
      put(aggrname, values);
    }//add

  }//inner class DummyAttributes


}//class DummyMessages
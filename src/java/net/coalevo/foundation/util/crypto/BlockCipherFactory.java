/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.Security;


/**
 * Provides a factory for block ciphers.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class BlockCipherFactory {

  private String m_Algorithm;
  private String m_Mode;
  private String m_Padding;
  private String m_Suite;
  private String m_Provider;

  static {
    if (Security.getProvider("CryptixCrypto") == null) {
      Provider p = new cryptix.jce.provider.CryptixCrypto();
      Security.addProvider(p);
    }
  }

  public BlockCipherFactory() {
    m_Algorithm = AES;
    m_Mode = CBC;
    m_Padding = PKCS5;
    updateSuite();
    m_Provider = "CryptixCrypto";
  }//constructor

  public BlockCipherFactory(String algo, String mode, String pad) {
    m_Algorithm = algo;
    m_Mode = mode;
    m_Padding = pad;
    updateSuite();
    m_Provider = "CryptixCrypto";
  }//BlockCipherFactory

  public BlockCipherFactory(String algo, String mode, String pad, String pro) {
    m_Algorithm = algo;
    m_Mode = mode;
    m_Padding = pad;
    updateSuite();
    m_Provider = pro;
  }//BlockCipherFactory

  public void setProvider(String p) {
    if (Security.getProvider(p) == null) {
      throw new IllegalArgumentException();
    }
    m_Provider = p;
  }//setProvider

  public Cipher createEncryptionCypher(byte[] key, byte[] iv) throws Exception {
    Cipher cipher =
        (m_Provider == null) ? Cipher.getInstance(m_Suite) : Cipher.getInstance(m_Suite, m_Provider);
    SecretKeySpec skeySpec = new SecretKeySpec(key, AES);
    IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParamSpec);
    return cipher;
  }//createEncryptionCipher

  public Cipher createDecryptionCypher(byte[] key, byte[] iv) throws Exception {
    Cipher cipher = (m_Provider == null) ? Cipher.getInstance(m_Suite) : Cipher.getInstance(m_Suite, m_Provider);
    SecretKeySpec skeySpec = new SecretKeySpec(key, AES);
    IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
    cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParamSpec);
    return cipher;
  }//createDecryptionCipher

  private void updateSuite() {
    StringBuilder sbuf = new StringBuilder();
    sbuf.append(m_Algorithm)
        .append("/")
        .append(m_Mode)
        .append("/")
        .append(m_Padding);
    m_Suite = sbuf.toString();
  }//updateSuite

  public static final String AES = "Rijndael"; //in Cryptix
  public static final String CBC = "CBC";
  public static final String PKCS5 = "PKCS7";

}//class BlockCipherFactory

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;

/**
 * Utility class for message digestion.
 * <p/>
 * This class will automatically register the embedded Cryptix Crypto
 * provider if this did not happen beforehand. It will use the
 * Tiger implementation as provided by the Cryptix crypto provider
 * as cryptographic hash algorithm (192-bit). The digests are pooled
 * and reused in a thread safe manner.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class DigestUtil {

  public static final MessageDigestPool Pool;

  static {
    if (Security.getProvider("CryptixCrypto") == null) {
      Provider p = new cryptix.jce.provider.CryptixCrypto();
      Security.addProvider(p);
    }
    // pool config
    GenericObjectPool.Config pcfg = new GenericObjectPool.Config();
    //set for
    pcfg.maxActive = 25;
    pcfg.maxIdle = 25;
    pcfg.maxWait = -1;
    pcfg.testOnBorrow = false;
    pcfg.testOnReturn = false;
    //make it growable so it adapts
    pcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    Pool = new MessageDigestPool(pcfg);
  }//static initializer

  public static byte[] digest(byte[] bytes) {
    MessageDigest md = Pool.lease();
    try {
      return md.digest(bytes);
    } finally {
      Pool.release(md);
    }
  }//digest

  public static String digestToString(byte[] bytes) {
    MessageDigest md = Pool.lease();
    try {
      return new String(Base64.encodeBase64(md.digest(bytes)));
    } finally {
      Pool.release(md);
    }
  }//digestToString

  public static String digestToString(String str) {
    try {
      return digestToString(str.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      //UTF-8 should be supported, quiet the compiler
      return null;
    }
  }//digestToString

  public static String digestToHexString(byte[] bytes) {
    MessageDigest md = Pool.lease();
    try {
      return new String(Hex.encodeHex(md.digest(bytes)));
    } finally {
      Pool.release(md);
    }
  }//digestToHexString

  public static String digestToHexString(String str) {
    try {
      return digestToHexString(str.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      //UTF-8 should be supported, quiet the compiler
      return null;
    }
  }//digestToHexString

  public static class MessageDigestPool extends GenericObjectPool {

    public MessageDigestPool(GenericObjectPool.Config cfg) {
      super(new MessageDigestFactory(), cfg);
    }//MacFactory

    public MessageDigest lease() {
      try {
        return (MessageDigest) this.borrowObject();
      } catch (Exception ex) {
        return null;
      }
    }//lease

    public void release(MessageDigest md) {
      try {
        this.returnObject(md);
      } catch (Exception ex) {

      }
    }//release

  }//MessageDigestPool

  static class MessageDigestFactory
      extends BasePoolableObjectFactory {

    public MessageDigestFactory() {
    }//constructor

    public Object makeObject() throws Exception {
      return MessageDigest.getInstance(DIGEST, "CryptixCrypto");
    }//makeObject

    public void passivateObject(Object o) {
      ((MessageDigest) o).reset();
    }//passivateObject

  }//inner class MessageDigestFactory

  public static void main(String[] args) {
    System.out.println(DigestUtil.digestToHexString(""));
    System.out.println(DigestUtil.digestToString(""));
    System.out.println(DigestUtil.digestToHexString("Test"));
    System.out.println(DigestUtil.digestToString("Test"));
  }//main

  public static final String DIGEST = "SHA-256";
  
}//class DigestUtil

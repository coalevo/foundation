/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.Security;

/**
 * Implements a Key Derivation Function based on the
 * description in
 * <a href="http://www.cs.ucdavis.edu/~rogaway/umac/2000/draft-krovetz-umac-01.txt">
 * UMAC
 * </a>
 * <p/>
 * This implementation is thread-safe.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class KDF {

  private Cipher m_Cipher;
  private byte[] m_Buffer;

  static {
    if (Security.getProvider("CryptixCrypto") == null) {
      Provider p = new cryptix.jce.provider.CryptixCrypto();
      Security.addProvider(p);
    }
  }//static initializer

  /**
   * Initializes the key derivation function.
   *
   * @param K   the original key (AES key lengths 128,192,256 bits)
   * @param idx the seed index.
   * @throws Exception if initialization fails.
   */
  public void init(byte[] K, byte idx) throws Exception {
    m_Cipher = Cipher.getInstance("Rijndael/ECB/NoPadding", "CryptixCrypto");
    SecretKeySpec skeySpec = new SecretKeySpec(K, BlockCipherFactory.AES);
    m_Cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
    m_Buffer = new byte[16];
    m_Buffer[15] = (byte) idx;
    nextKey();
  }//init

  /**
   * Returns a 256-bit derived from the original key.
   *
   * @return a 256-bit key as <tt>byte[]</tt>.
   * @throws Exception if the key cannot be derived.
   */
  public byte[] nextKey() throws Exception {

    byte[] out = new byte[32];
    byte[] im = new byte[16];
    synchronized (m_Buffer) {
      im = m_Cipher.doFinal(m_Buffer);
      m_Buffer = xor(m_Buffer, im);

      System.arraycopy(m_Buffer, 0, out, 0, 16);
      m_Buffer = m_Cipher.doFinal(m_Buffer);
      m_Buffer = xor(m_Buffer, im);

      System.arraycopy(m_Buffer, 0, out, 16, 16);
    }
    return out;
  }//next

  /**
   * Returns a key depending on the requested length.
   *
   * @param len if 128 a 128-bit key will be returned, in all other cases, a
   *            256 bit key will be returned.
   * @return a 128-bit or a 256-bit key.
   * @throws Exception if the key cannot be derived.
   */
  public byte[] nextKey(int len) throws Exception {
    if (len == 128) {
      byte[] out = new byte[16];
      synchronized (m_Buffer) {
        out = m_Cipher.doFinal(m_Buffer);
        m_Buffer = xor(out, m_Buffer);
        System.arraycopy(m_Buffer, 0, out, 0, 16);
      }
      return out;
    } else {
      return nextKey();
    }
  }//nextKey

  private static final byte[] xor(byte[] a, byte[] b) {
    int len = a.length;
    if (b.length != len) {
      throw new IllegalArgumentException();
    }
    byte[] r = new byte[len];
    for (int i = 0; i < len; i++) {
      r[i] = (byte) (a[i] ^ b[i]);
    }
    return r;
  }//xor


  public static void main(String[] args) {
    try {
      KDF umac = new KDF();
      byte[] bytes = new byte[16];
      //new java.util.Random().nextBytes(bytes);
      bytes = new byte[]{1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4};
      umac.init(bytes, (byte)3);
      System.out.println(new String(Hex.encodeHex(umac.nextKey())));
      System.out.println(new String(Hex.encodeHex(umac.nextKey())));
      System.out.println(new String(Hex.encodeHex(umac.nextKey())));

      System.out.println(umac.m_Buffer.length);

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  

}//class KDF

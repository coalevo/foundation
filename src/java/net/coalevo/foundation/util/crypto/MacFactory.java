/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.Security;

/**
 * Factory for MAC classes.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class MacFactory {

  static {
    if (Security.getProvider("CryptixCrypto") == null) {
      Provider p = new cryptix.jce.provider.CryptixCrypto();
      Security.addProvider(p);
    }
  }//initializer


  private MacFactory() {
  }//MacFactory

  public static Mac createMac(byte[] key) {
    try {
      Mac mac = Mac.getInstance(DEFAULT_MAC, DEFAULT_PROVIDER);
      mac.init(new SecretKeySpec(key, DEFAULT_CRYPT));
      return mac;
    } catch (Exception ex) {
      ex.printStackTrace();
      return null;
    }
  }//createMac

  public static Mac createMac(String provider, String macn, String crypt, byte[] key) {
    try {
      Mac mac = Mac.getInstance(macn, provider);
      mac.init(new SecretKeySpec(key, crypt));
      return mac;
    } catch (Exception ex) {
      return null;
    }
  }//createMac

  private static final String DEFAULT_PROVIDER = "CryptixCrypto";
  private static final String DEFAULT_MAC = "HMAC-SHA256";
  private static final String DEFAULT_CRYPT = "Rijndael";

}//class MacFactory

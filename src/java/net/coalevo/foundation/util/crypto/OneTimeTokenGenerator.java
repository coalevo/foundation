/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import org.apache.commons.codec.binary.Hex;

/**
 * This class implements a one time token generator.
 * <p/>
 * The generation of tokens is based on the key derivation function
 * implemented by {@link KDF} and a 192-bit Tiger hash.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class OneTimeTokenGenerator {

  private KDF m_KDF;

  public void init(byte[] key, byte seed) {
    m_KDF = new KDF();
    try {
      m_KDF.init(key, seed);
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
    }
  }//init

  public String nextToken() {
    try {
      byte[] b = m_KDF.nextKey();
      return new String(Hex.encodeHex(DigestUtil.digest(b)));
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
    }
    return "";
  }//nextToken

  public byte[] nextTokenRaw() {
    try {
      return m_KDF.nextKey();
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
    }
    return new byte[]{};
  }//nextTokenRaw

  public static void main(String[] args) {
    OneTimeTokenGenerator o = new OneTimeTokenGenerator();
    byte[] key = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    o.init(key,(byte)1);
    long start = System.currentTimeMillis();
    for(int i = 0; i<3;i++) {
      //o.nextToken();
      System.out.println(o.nextToken());
    }
    System.out.println(System.currentTimeMillis()-start);
  }//main

}//class OneTimeTokenGenerator

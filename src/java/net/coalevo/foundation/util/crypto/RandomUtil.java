/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import cryptix.jce.provider.random.DevRandom;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.security.GeneralSecurityException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;

/**
 * Provides utility methods to efficiently obtain pseudo random
 * bytes or values.
 * <p/>
 *  This class will automatically register the embedded Cryptix Crypto
 * provider if this did not happen beforehand. It will use the
 * SecureRandom implementation as provided by the Cryptix crypto provider.
 * The SecureRandom instances are pooled and reused in a thread safe manner.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class RandomUtil {

  public static final RandomPool c_RandomPool;
  private static SecureRandomFactory c_Factory;


  static {
    if (DevRandom.isAvailable() && Security.getProvider("CryptixRandom") == null) {
      Provider p = new cryptix.jce.provider.CryptixRandom();
      Security.addProvider(p);
    }
    // pool config
    GenericObjectPool.Config pcfg = new GenericObjectPool.Config();
    //set for
    pcfg.maxActive = 25;
    pcfg.maxIdle = 25;
    pcfg.maxWait = -1;
    pcfg.testOnBorrow = false;
    pcfg.testOnReturn = false;
    //make it growable so it adapts
    pcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    c_Factory = new SecureRandomFactory();
    c_RandomPool = new RandomPool(c_Factory, pcfg);
  }//init

  private RandomUtil() {
  }//RandomUtil

  public static void nextBytes(byte[] b) {
    SecureRandom rnd = c_RandomPool.lease();
    try {
      rnd.nextBytes(b);
    } finally {
      c_RandomPool.release(rnd);
    }
  }//nextBytes

  public static void nextBytes(byte[] b, int off, int len) {
    System.arraycopy(getBlock(len), 0, b, off, len);
  }//nextBytes

  public static byte nextByte() {
    return getBlock(1)[0];
  }//nextByte

  public static byte nextIndex() {
    byte b = 0;
    do {
      b = getBlock(1)[0];
    } while (b == 0);
    return b;
  }//nextIndex

  public static byte[] getBlock(int size) {
    byte[] iv = new byte[size];
    nextBytes(iv);
    return iv;
  }//IV

  private static class RandomPool extends GenericObjectPool {

    public RandomPool(PoolableObjectFactory factory, Config config) {
      super(factory, config);
    }//RandomPool

    public SecureRandom lease() {
      try {
        return (SecureRandom) borrowObject();
      } catch (Exception ex) {
        try {
          return (SecureRandom) c_Factory.makeObject();
        } catch (Exception e) {
          return null;
        }
      }
    }//lease

    public void release(SecureRandom rnd) {
      try {
        returnObject(rnd);
      } catch (Exception ex) {
      }
    }//release
  }

  private static class SecureRandomFactory
      extends BasePoolableObjectFactory {

    public SecureRandomFactory() {
    }//constructor

    public Object makeObject() throws Exception {
      try{
        return SecureRandom.getInstance("DevRandom", "CryptixRandom");
      } catch (GeneralSecurityException gsex) {
        return new SecureRandom();
      }
    }//makeObject

  }//inner class MacFactory

}//class RandomUtil

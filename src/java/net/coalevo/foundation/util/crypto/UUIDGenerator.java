/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.crypto;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.net.InetAddress;

/**
 * Provides a UID generator, that generates alphanumeric
 * identifiers with a random component (padding), a temporal
 * component (system time) and a spatial component
 * (IPv4 address bytes).
 * <p/>
 * For constant size, the combination of the components is hashed
 * with the {@link DigestUtil} default hash.
 * <p/>
 * This implementation uses a highly optimized and strong
 * random number generator implementation (MersenneTwister).
 * Buffers are pooled and recycled, generation methods can
 * be used concurrently.
 *
 * @author Dieter Wimberger (coalevo)
 * @version 0.0.1 (30/04/2006)
 */
public final class UUIDGenerator {

  //class attributes
  private static final RandomGenerator c_RndGen;
  private static final RandomSeedGenerator c_RndSeed;
  private static int c_ReseedCounter = 0;
  private static byte[] c_SpatialBytes;
  private static final int RANDOM_RESEED = 10000;
  private static final int UID_LENGTH = 156; //3 per block
  private static GenericObjectPool c_RawBuffers;
  private static byte[] c_BufferPlaceHolder = new byte[0];

  //create seeded random
  static {
    //1. Init Random Generator instances
    c_RndGen = new RandomGenerator();
    c_RndSeed = new RandomSeedGenerator();
    seedRandom(); //prepare call
    c_RndGen.nextBlock(); //prepare call
    //3. prepare spatial bytes
    try {
      c_SpatialBytes = InetAddress.getLocalHost().getAddress();
    } catch (Exception ex) {
      c_SpatialBytes = c_RndGen.nextBytes(4, 0, new byte[4]);
    }
    //4. prepare raw buffer
    GenericObjectPool.Config poolConfig = new GenericObjectPool.Config();
    poolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolConfig.maxActive = 10;
    poolConfig.maxIdle = 10;
    poolConfig.maxWait = -1;
    poolConfig.timeBetweenEvictionRunsMillis = 1000 * 60; //1 per min
    c_RawBuffers = new GenericObjectPool(
        new BasePoolableObjectFactory() {
          public Object makeObject() throws Exception {
            return new byte[UID_LENGTH];
          }//makeObject
        },
        poolConfig);
  }//class initializer

  private UUIDGenerator() {
  }//constructor

  /**
   * Returns a UID (unique identifier) as <tt>String</tt>.
   * The returned string is a hex representation of a raw
   * UID.
   *
   * @return a UID as <tt>String</tt>.
   */
  public static final String getUID() {
    return new String(Base64.encodeBase64(getRawUID()));
  }//getUID

  /**
   * Returns a UID (unique identifier) as <tt>String</tt>.
   * The identifier represents the MD5 hashed combination
   * of a completely random padding, a temporal (system time)
   * and a spatial (IP address) component.
   *
   * @return a raw UID as <tt>byte[]</tt>.
   */
  public static final byte[] getRawUID() {
    byte[] buf = c_BufferPlaceHolder;

    try {
      if (c_ReseedCounter == RANDOM_RESEED) {
        seedRandom();
        c_ReseedCounter = 0;
      } else {
        c_ReseedCounter++;
      }
      buf = (byte[]) c_RawBuffers.borrowObject();
      c_RndGen.nextBytes(UID_LENGTH, 0, buf);
      //RandomUtil.nextBytes(buf,0,UID_LENGTH);
      //8 time bytes
      longToBytes(System.currentTimeMillis(), buf, 21);
      //4 spatial bytes
      System.arraycopy(c_SpatialBytes, 0, buf, 52, c_SpatialBytes.length);
      //random padding
      return DigestUtil.digest(buf);

    } catch (Exception ex) {

    } finally {
      try {
        c_RawBuffers.returnObject(buf);
      } catch (Exception ex) {
      }
    }
    return null;

  }//getRawUID

  /**
   * Places a long value as bytes into a given buffer at
   * the given offset.
   *
   * @param v   the value to be converted.
   * @param buf the buffer to place the bytes.
   * @param off the offset where to start placing bytes from.
   */
  private static final void longToBytes(long v, byte[] buf, int off) {
    buf[off] = (byte) (0xff & (v >> 56));
    buf[off + 1] = (byte) (0xff & (v >> 48));
    buf[off + 2] = (byte) (0xff & (v >> 40));
    buf[off + 3] = (byte) (0xff & (v >> 32));
    buf[off + 4] = (byte) (0xff & (v >> 24));
    buf[off + 5] = (byte) (0xff & (v >> 16));
    buf[off + 6] = (byte) (0xff & (v >> 8));
    buf[off + 7] = (byte) (0xff & v);
  }//longToBytes

  public static final void seedRandom() {
    c_RndGen.setSeed(c_RndSeed.nextSeed());
  }//seedRandom

  public static void main(String[] args) {
    long sum = 0;
    //burn in
    System.out.println("Burn In " + getUID());
    System.out.println("Burn In " + getUID());
    System.out.println("Burn In " + getUID());
    System.out.println("Burn In " + getUID());
    System.out.println("Burn In " + getUID());
    System.out.println("Burn In " + getUID());
    for (int i = 0; i < 1000; i++) {
      getUID();
    }
    for (int i = 0; i < 10000; i++) {
      long start = System.currentTimeMillis();
      getUID();
      sum += System.currentTimeMillis() - start;
    }
    System.out.println("Total generation time " + sum + " [ms], average per generation " + (sum / 10000) + " [ms]");
  }
}//UIDGenerator
/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.metatype;

import org.osgi.service.metatype.AttributeDefinition;


/**
 * Exception thrown by a {@link MetaTypeDictionary} if the
 * requested attribute is not of requested type.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AttributeTypeException
    extends MetaTypeDictionaryException {

  private int m_Type;
  private int m_RequestedType;
  private String m_Key;

  public AttributeTypeException() {
  }//constructor

  public AttributeTypeException(String message) {
    super(message);
  }//constructor(String)

  public AttributeTypeException(String key, int type, int reqtype) {
    m_Key = key;
    m_Type = type;
    m_RequestedType = reqtype;
  }//AttributeTypeException

  public int getType() {
    return m_Type;
  }//getType

  public void setType(int type) {
    m_Type = type;
  }//setType

  public int getRequestedType() {
    return m_RequestedType;
  }

  public void setRequestedType(int requestedType) {
    m_RequestedType = requestedType;
  }

  public String getKey() {
    return m_Key;
  }

  public void setKey(String key) {
    m_Key = key;
  }

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final AttributeTypeException that = (AttributeTypeException) o;

    if (m_RequestedType != that.m_RequestedType) return false;
    if (m_Type != that.m_Type) return false;
    if (m_Key != null ? !m_Key.equals(that.m_Key) : that.m_Key != null) return false;

    return true;
  }//equals

  public int hashCode() {
    int result;
    result = m_Type;
    result = 29 * result + m_RequestedType;
    result = 29 * result + (m_Key != null ? m_Key.hashCode() : 0);
    return result;
  }//hashCode

  public String getTypeName(int type) {
    switch (type) {
      case AttributeDefinition.BOOLEAN:
        return "Boolean";
      case AttributeDefinition.BYTE:
        return "Byte";
      case AttributeDefinition.CHARACTER:
        return "Character";
      case AttributeDefinition.DOUBLE:
        return "Double";
      case AttributeDefinition.FLOAT:
        return "Float";
      case AttributeDefinition.INTEGER:
        return "Integer";
      case AttributeDefinition.LONG:
        return "Long";
      case AttributeDefinition.SHORT:
        return "Short";
      case AttributeDefinition.STRING:
        return "String";
      default:
        return "unknown";
    }
  }//getTypeName

}//class AttributeTypeException

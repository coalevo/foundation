/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.metatype;

import net.coalevo.foundation.impl.Activator;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.metatype.AttributeDefinition;
import org.osgi.service.metatype.MetaTypeInformation;
import org.osgi.service.metatype.MetaTypeService;
import org.osgi.service.metatype.ObjectClassDefinition;

import java.util.*;

/**
 * Provides a MetaType aware wrapper for configuration
 * dictionaries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MetaTypeDictionary {

  //TODO: Add array support
  //TODO: Enhance poor Vector support

  private Dictionary m_Dictionary;
  private HashMap m_Definitions;

  /**
   * Constructs a new instance wrapping the given <tt>Dictionary<tt>
   * and <tt>ObjectClassDefinition</tt>.
   *
   * @param d   the <tt>Dictionary</tt> to be wrapped.
   * @param ocd the <tt>ObjectClassDefinition</tt> of the given <tt>Dictionary</tt>.
   */
  public MetaTypeDictionary(Dictionary d, ObjectClassDefinition ocd) {
    m_Dictionary = d;
    m_Definitions = new HashMap();
    prepareDefinitions(ocd);
  }//MetaTypeDictionary

  /**
   * Creates a new instance wrapping the given <tt>Dictionary</tt>.
   * <p/>
   * MetaTypeInformation will be obtained from the MetaTypeService
   * for the given PID.
   * </p>
   *
   * @param d   the <tt>Dictionary</tt> to be wrapped.
   * @param bc  a <tt>BundleContext</tt> for a specific requesting bundle.
   * @param pid the PID corresponding to a MetaType designate of the bundle.
   */
  public MetaTypeDictionary(Dictionary d, BundleContext bc, String pid) {
    m_Dictionary = d;
    m_Definitions = new HashMap();
    final MetaTypeService mts = getMetaTypeService(bc);
    if (mts == null) {
      throw new UnsupportedOperationException(
          Activator.getBundleMessages().get("MetaTypeDictionary.exception.noservice"));
    }
    final MetaTypeInformation mi = mts.getMetaTypeInformation(bc.getBundle());
    if (mi == null) {
      throw new UnsupportedOperationException(
          Activator.getBundleMessages().get("MetaTypeDictionary.exception.noinfo"));
    }
    final ObjectClassDefinition ocd =
        mi.getObjectClassDefinition(pid, localeToString(Locale.getDefault()));
    //this actually should never happen!
    if (ocd == null) {
      throw new UnsupportedOperationException(
          Activator.getBundleMessages().get("MetaTypeDictionary.exception.noocd"));
    }
    prepareDefinitions(ocd);
  }//MetaTypeDictionary

  /**
   * Creates a new instance wrapping the given <tt>Dictionary</tt>.
   * <p/>
   * MetaTypeInformation will be obtained from the MetaTypeService
   * for the given PID and Bundle.
   * </p>
   *
   * @param d   the <tt>Dictionary</tt> to be wrapped.
   * @param bc  a <tt>BundleContext</tt> to obtain the MetaTypeService.
   * @param b   the <tt>Bundle</tt> the PID corresponds to
   * @param pid the PID corresponding to a MetaType designate of the bundle.
   */
  public MetaTypeDictionary(Dictionary d, BundleContext bc, Bundle b, String pid) {
    m_Dictionary = d;
    m_Definitions = new HashMap();
    final MetaTypeService mts = getMetaTypeService(bc);
    if (mts == null) {
      throw new UnsupportedOperationException(
          Activator.getBundleMessages().get("MetaTypeDictionary.exception.noservice"));
    }
    final MetaTypeInformation mi = mts.getMetaTypeInformation(b);
    if (mi == null) {
      throw new UnsupportedOperationException(
          Activator.getBundleMessages().get("MetaTypeDictionary.exception.noinfo"));
    }
    final ObjectClassDefinition ocd =
        mi.getObjectClassDefinition(pid, localeToString(Locale.getDefault()));
    //this actually should never happen!
    if (ocd == null) {
      throw new UnsupportedOperationException(
          Activator.getBundleMessages().get("MetaTypeDictionary.exception.noocd"));
    }
    prepareDefinitions(ocd);
  }//MetaTypeDictionary

  /**
   * Sets the wrapped <tt>Dictionary</tt>.
   *
   * @param d a <tt>Dictionary</tt> instance.
   */
  public void setDictionary(Dictionary d) {
    m_Dictionary = d;
  }//setDictionary

  /**
   * Returns the wrapped <tt>Dictionary</tt>.
   * <p/>
   * Note that on the first call this method will return an instance
   * populated with defaults, if no <tt>Dictionary</tt>
   * instance is available.
   * </p>
   *
   * @return a <tt>Dictionary</tt> instance.
   */
  public Dictionary getDictionary() {
    if (m_Dictionary == null) {
      m_Dictionary = new Properties();
      for (Iterator iter = m_Definitions.entrySet().iterator(); iter.hasNext();) {
        Map.Entry entry = (Map.Entry) iter.next();
        String key = (String) entry.getKey();
        AttributeDefinition ad = (AttributeDefinition) entry.getValue();
        try {
          switch (ad.getType()) {
            case AttributeDefinition.BOOLEAN:
              m_Dictionary.put(key, getBoolean(key));
              break;
            case AttributeDefinition.BYTE:
              m_Dictionary.put(key, getByte(key));
              break;
            case AttributeDefinition.CHARACTER:
              m_Dictionary.put(key, getCharacter(key));
              break;
            case AttributeDefinition.DOUBLE:
              m_Dictionary.put(key, getDouble(key));
              break;
            case AttributeDefinition.FLOAT:
              m_Dictionary.put(key, getFloat(key));
              break;
            case AttributeDefinition.INTEGER:
              m_Dictionary.put(key, getInteger(key));
              break;
            case AttributeDefinition.LONG:
              m_Dictionary.put(key, getLong(key));
              break;
            case AttributeDefinition.SHORT:
              m_Dictionary.put(key, getShort(key));
              break;
            case AttributeDefinition.STRING:
              m_Dictionary.put(key, getString(key));
              break;
            default:
              System.err.println("MetaTypeDictionary unsupported type.");
              break;
          }
        } catch (MetaTypeDictionaryException mex) {
          //shouldnt really happen!
        }
      }
    }
    return m_Dictionary;
  }//getDictionary

  /**
   * Returs a <tt>Set</tt> holding the names of the
   * attributes defined for this <tt>MetaTypeDictionary</tt>.
   *
   * @return a <tt>Set</tt> of Strings representing the attribute names.
   */
  public Set keySet() {
    return m_Definitions.keySet();
  }//keySet

  /**
   * Returns the type of a named attribute.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return a type as defined in <tt>AttributeDefinition</tt>
   */
  public int getType(String key)
      throws NoSuchAttributeException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      return ad.getType();
    }
  }//getType

  /**
   * Returns the <tt>AttributeDefinition</tt> of a named
   * attribute.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return an <tt>AttributeDefinition</tt> instance.
   * @throws NoSuchAttributeException if an attribute with the given name does
   *                                  not exist.
   */
  public AttributeDefinition getDefinition(String key)
      throws NoSuchAttributeException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      return (AttributeDefinition) o;
    }
  }//getAttributeDefinition

  /**
   * Sets the value of a specific attribute converting to the
   * defined corresponding type.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param val the value to be set as <tt>String</tt>.
   */
  public void setValue(String key, String val)
      throws NoSuchAttributeException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      m_Dictionary.put(key, convert(val, ad.getType()));
    }
  }//setValue

  /**
   * Returns the value of a specific attribute converting to
   * a <tt>String</tt>.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the value as <tt>String</tt>.
   */
  public String getValue(String key)
      throws MetaTypeDictionaryException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      switch (ad.getType()) {
        case AttributeDefinition.BOOLEAN:
          return getBoolean(key).toString();
        case AttributeDefinition.BYTE:
          return getByte(key).toString();
        case AttributeDefinition.CHARACTER:
          return getCharacter(key).toString();
        case AttributeDefinition.DOUBLE:
          return getDouble(key).toString();
        case AttributeDefinition.FLOAT:
          return getFloat(key).toString();
        case AttributeDefinition.INTEGER:
          return getInteger(key).toString();
        case AttributeDefinition.LONG:
          return getLong(key).toString();
        case AttributeDefinition.SHORT:
          return getShort(key).toString();
        case AttributeDefinition.STRING:
          return getString(key);
        default:
          return null;
      }
    }
  }//getValue

  /**
   * Returns the values associated with the given key.
   * <p/>
   * This method is a helper to access multi-value attributes
   * stored as vector. Note that you will be returned the orginal
   * reference, so updates to the vector will be reflected in the
   * associated <tt>Dictionary</tt>. Please take care that you
   * observe the <tt>AttributeDefinition</tt> with regards to the
   * class (don't mix) and the cardinality (if limited).
   * </p>
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Boolean</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Vector getValueVector(String key)
      throws MetaTypeDictionaryException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      final int cardinality = ad.getCardinality();
      if (cardinality >= 0) {
        throw new MetaTypeDictionaryException("Cardinality");
      } else {
        Object val = m_Dictionary.get(key);
        if (val != null && val instanceof Vector) {
          return (Vector) val;
        } else {
          //try and get the default values
          final String[] def = ad.getDefaultValue();
          final Vector v = new Vector();
          for (int i = 0; def != null && i < def.length && i < cardinality; i++) {
            v.addElement(def[i]);
          }
          m_Dictionary.put(key, v);
          return v;
        }
      }
    }
  }//getValueVector

  /**
   * Returns the <tt>Boolean</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Boolean</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Boolean getBoolean(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.BOOLEAN);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      return (Boolean) convert(ad.getDefaultValue()[0], AttributeDefinition.BOOLEAN);
    } else {
      return ((Boolean) o);
    }
  }//getBoolean

  /**
   * Sets the <tt>Boolean</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param b   the <tt>Boolean</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setBoolean(String key, Boolean b)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.BOOLEAN);
    if (validateOption(key, b)) {
      m_Dictionary.put(key, b);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setBoolean

  /**
   * Returns the <tt>Byte</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Byte</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Byte getByte(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.BYTE);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      return (Byte) convert(ad.getDefaultValue()[0], AttributeDefinition.BYTE);
    } else {
      return ((Byte) o);
    }
  }//getByte

  /**
   * Sets the <tt>Byte</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param b   the <tt>Byte</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setByte(String key, Byte b)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.BYTE);
    if (validateOption(key, b)) {
      m_Dictionary.put(key, b);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setByte

  /**
   * Returns the <tt>Character</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Character</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Character getCharacter(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.CHARACTER);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      final String[] dv = ad.getDefaultValue();
      if (dv == null || dv.length == 0 || dv[0].length() == 0) {
        return new Character(Character.MIN_VALUE);
      } else {
        return (Character) convert(dv[0], AttributeDefinition.CHARACTER);
      }
    } else {
      return ((Character) o);
    }
  }//getCharacter

  /**
   * Sets the <tt>Character</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param c   the <tt>Character</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setCharacter(String key, Character c)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.CHARACTER);
    if (validateOption(key, c)) {
      m_Dictionary.put(key, c);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setCharacter


  /**
   * Returns the <tt>Double</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Double</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Double getDouble(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.DOUBLE);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      final String[] dv = ad.getDefaultValue();
      if (dv == null || dv.length == 0 || dv[0].length() == 0) {
        return new Double(Double.MIN_VALUE);
      } else {
        return (Double) convert(dv[0], AttributeDefinition.DOUBLE);
      }
    } else {
      return ((Double) o);
    }
  }//getDouble

  /**
   * Sets the <tt>Double</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param d   the <tt>Double</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setDouble(String key, Double d)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.DOUBLE);
    if (validateOption(key, d)) {
      m_Dictionary.put(key, d);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setDouble

  /**
   * Returns the <tt>Float</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Float</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Float getFloat(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.FLOAT);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      final String[] dv = ad.getDefaultValue();
      if (dv == null || dv.length == 0 || dv[0].length() == 0) {
        return new Float(Float.MIN_VALUE);
      } else {
        return (Float) convert(dv[0], AttributeDefinition.FLOAT);
      }
    } else {
      return ((Float) o);
    }
  }//getFloat

  /**
   * Sets the <tt>Float</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param f   the <tt>Float</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setFloat(String key, Float f)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.FLOAT);
    if (validateOption(key, f)) {
      m_Dictionary.put(key, f);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setFloat

  /**
   * Returns the <tt>Integer</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Integer</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Integer getInteger(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.INTEGER);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      final String[] dv = ad.getDefaultValue();
      if (dv == null || dv.length == 0 || dv[0].length() == 0) {
        return new Integer(Integer.MIN_VALUE);
      } else {
        return (Integer) convert(dv[0], AttributeDefinition.INTEGER);
      }
    } else {
      return ((Integer) o);
    }
  }//getInteger


  /**
   * Sets the <tt>Integer</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param i   the <tt>Integer</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setInteger(String key, Integer i)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.INTEGER);
    if (validateOption(key, i)) {
      m_Dictionary.put(key, i);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setInteger

  /**
   * Returns the <tt>Long</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Long</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Long getLong(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.LONG);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      final String[] dv = ad.getDefaultValue();
      if (dv == null || dv.length == 0 || dv[0].length() == 0) {
        return new Long(Long.MIN_VALUE);
      } else {
        return (Long) convert(dv[0], AttributeDefinition.LONG);
      }
    } else {
      return ((Long) o);
    }
  }//getLong

  /**
   * Sets the <tt>Long</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param l   the <tt>Long</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setLong(String key, Long l)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.LONG);
    if (validateOption(key, l)) {
      m_Dictionary.put(key, l);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setLong

  /**
   * Returns the <tt>Short</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>Short</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public Short getShort(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.SHORT);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      final String[] dv = ad.getDefaultValue();
      if (dv == null || dv.length == 0 || dv[0].length() == 0) {
        return new Short(Short.MIN_VALUE);
      } else {
        return (Short) convert(dv[0], AttributeDefinition.SHORT);
      }
    } else {
      return ((Short) o);
    }
  }//getShort

  /**
   * Sets the <tt>Short</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param s   the <tt>Short</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public void setShort(String key, Short s)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.SHORT);
    if (validateOption(key, s)) {
      m_Dictionary.put(key, s);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setShort


  /**
   * Returns the <tt>String</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return the stored or default <tt>String</tt> value.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public String getString(String key)
      throws MetaTypeDictionaryException {
    //1. get the attribute definition with type check
    final AttributeDefinition ad = getAttributeDefinition(key, AttributeDefinition.STRING);
    //2. get and return the value
    final Object o = m_Dictionary.get(key);
    if (o == null) {
      if(ad == null) {
        //System.out.println("MetaTypeDictionary::getString("+key+")::ad==null");
        return "";
      }
      String[] values = ad.getDefaultValue();
      if(values == null) {
        //System.out.println("MetaTypeDictionary::getString("+key+")::defaultvalues==null");
        return "";
      } else {
        return values[0];
      }
      //return ad.getDefaultValue()[0];
    } else {
      return (String) o;
    }
  }//getString

  /**
   * Sets the <tt>String</tt> value associated with the given key.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @param val the <tt>String</tt> value to be set.
   * @throws MetaTypeDictionaryException if the attribute is not defined
   *                                     or the defined type is not correct.
   */
  public final void setString(String key, String val)
      throws MetaTypeDictionaryException {
    //check type definition
    getAttributeDefinition(key, AttributeDefinition.STRING);
    if (validateOption(key, val)) {
      m_Dictionary.put(key, val);
    } else {
      throw new MetaTypeDictionaryException("Invalid option.");
    }
  }//setString

  /**
   * Removes the value associated with the given key.
   * <p/>
   * The default value will be returned for the next get of the
   * corresponding attribute.
   * </p>
   *
   * @param key a <tt>String</tt> representing an attribute name.
   */
  public void remove(String key) {
    m_Dictionary.remove(key);
  }//remove

  /**
   * Tests if a given attribute is defined with options.
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return true if there are options defined, false otherwise.
   * @throws NoSuchAttributeException if an attribute with the given name
   *                                  is not defined.
   */
  public boolean hasOptions(String key)
      throws NoSuchAttributeException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      String[] opts = ad.getOptionLabels();
      return (opts != null && opts.length > 0);
    }
  }//hasOptions

  /**
   * Returns a <tt>Map</tt> with the option labels
   * and option values (value).
   * <p/>
   * The values in the <tt>Map</tt> will correspond to
   * the native type wrappers of the type defined for the attribute.
   * </p>
   *
   * @param key a <tt>String</tt> representing an attribute name.
   * @return a <tt>Map</tt> instance with option labels as keys and option values as values,
   *         or null if no options are available for the specified attribute.
   * @throws NoSuchAttributeException if an attribute with the given name
   *                                  is not defined.
   */
  public Map getOptions(String key)
      throws NoSuchAttributeException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      String[] ol = ad.getOptionLabels();
      String[] ov = ad.getOptionValues();
      int type = ad.getType();
      if (ol != null && ol.length > 0) {
        final Map m = new HashMap();
        for (int i = 0; i < ol.length; i++) {
          m.put(ol[i], convert(ov[i], type));
        }
        return m;
      }
    }
    return null;
  }//getOptions

  protected boolean validateOption(String attkey, Object optvalue)
      throws MetaTypeDictionaryException {
    if (attkey == null || optvalue == null) {
      throw new MetaTypeDictionaryException();
    }
    final Map m = getOptions(attkey);
    if (m == null) {
      return true; //all are valid
    } else {
      for (Iterator iterator = m.values().iterator(); iterator.hasNext();) {
        Object o = iterator.next();
        if (optvalue.equals(o)) {
          return true;
        }
      }
    }
    return false;
  }//validateOption

  protected Object convert(String value, int type) {
    switch (type) {
      case AttributeDefinition.BOOLEAN:
        return new Boolean(value);
      case AttributeDefinition.BYTE:
        return new Byte(value);
      case AttributeDefinition.CHARACTER:
        return new Character(value.charAt(0));
      case AttributeDefinition.DOUBLE:
        return new Double(value);
      case AttributeDefinition.FLOAT:
        return new Float(value);
      case AttributeDefinition.INTEGER:
        return new Integer(value);
      case AttributeDefinition.LONG:
        return new Long(value);
      case AttributeDefinition.SHORT:
        return new Short(value);
      case AttributeDefinition.STRING:
        return value;
      default:
        return value;
    }
  }//convert

  protected AttributeDefinition getAttributeDefinition(String key, int type)
      throws NoSuchAttributeException, AttributeTypeException {
    final Object o = m_Definitions.get(key);
    if (o == null) {
      throw new NoSuchAttributeException(key);
    } else {
      final AttributeDefinition ad = (AttributeDefinition) o;
      if (ad.getType() != type) {
        throw new AttributeTypeException(key);
      } else {
        return ad;
      }
    }
  }//getAttributeDefinition

  protected void prepareDefinitions(ObjectClassDefinition ocd) {
    AttributeDefinition[] ads =
        ocd.getAttributeDefinitions(ObjectClassDefinition.ALL);
    if (ads == null) {
      return;
    }
    for (int i = 0; i < ads.length; i++) {
      AttributeDefinition ad = ads[i];
      m_Definitions.put(ad.getID(), ad);
    }
  }//prepareDefinitions

  private MetaTypeService getMetaTypeService(BundleContext bc) {
    ServiceReference sr =
        bc.getServiceReference(MetaTypeService.class.getName());
    if (sr == null) {
      return null;
    } else {
      Object o = bc.getService(sr);
      if (o != null) {
        return (MetaTypeService) o;
      } else {
        return null;
      }
    }
  }//getMessageResourceService

  private static String localeToString(Locale l) {
    final String lang = l.getLanguage();
    final String cntry = l.getCountry();
    final String var = l.getVariant();

    final StringBuilder sbuf = new StringBuilder();
    if (lang != null && lang.length() > 0) {
      sbuf.append(lang);
      if (cntry != null && cntry.length() > 0) {
        sbuf.append(LOCSEP);
        sbuf.append(cntry);
        if (var != null && var.length() > 0) {
          sbuf.append(LOCSEP);
          sbuf.append(var);
        }
      }
    }

    return sbuf.toString().toLowerCase();
  }//localeToString

  private static final String LOCSEP = "_";
}//class MetaTypeDictionary

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.srp;

import java.math.BigInteger;

/**
 * Provides an SRP helper for SRP client side.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class ClientSRP {

  private String m_Username;
  private String m_Password;
  private BigInteger m_SecretEphemeral; //a
  private BigInteger m_PublicEphemeral; //A
  private BigInteger m_ServerPublicEphemeral; //B
  private byte[] m_SessionKey;
  private String m_KeyMatchProof;

  public ClientSRP(String username, String password) {
    m_Username = username;
    m_Password = password;
    //init
    m_SecretEphemeral = SRP.getRandomBigInteger(32);
    m_PublicEphemeral = SRP.getClientPublicEphemeral(m_SecretEphemeral);
  }//ClientSRP

  /**
   * Returns the username.
   *
   * @return the username.
   */
  public String getUsername() {
    return m_Username;
  }//getUsername

  /**
   * Returns the public client ephemeral value.
   *
   * @return a String of hex encoded bytes representing a BigInteger value.
   */
  public String getPublicEphemeral() {
    return m_PublicEphemeral.toString(16);
  }//getPublicEphemeral

  /**
   * Should be called when the server returned the session init.
   *
   * @param salt the users salt.
   * @param Bhex the server public ephemeral as hex encoded BigInteger value.
   * @return false if aborted due to safeguards or key generation errors.
   */
  public boolean received(String salt, String Bhex) {
    m_ServerPublicEphemeral = new BigInteger(Bhex, 16);
    if (m_ServerPublicEphemeral.equals(SRP.c_Zero)) {
      return false;
    }
    //calculate random scrambling
    BigInteger u = SRP.getRandomScramblingParameter(m_PublicEphemeral, m_ServerPublicEphemeral);
    if (u.equals(SRP.c_Zero)) {
      return false;
    }
    //calulate key
    try {
      m_SessionKey = SRP.getClientSessionKey(salt, m_Password, m_ServerPublicEphemeral, m_SecretEphemeral, u);
    } catch (Exception ex) {
      return false;
    }
    m_KeyMatchProof = SRP.getClientKeyMatchProof(m_Username, salt, m_PublicEphemeral, m_ServerPublicEphemeral, m_SessionKey);
    return true;
  }//received

  /**
   * Return the client key match proof.
   *
   * @return a client key match proof.
   */
  public String getKeyMatchProof() {
    return m_KeyMatchProof;
  }//getKeyMatchProof

  /**
   * Tests the server key proof.
   *
   * @param proof the proof received from the server.
   * @return true if ok, false otherwise.
   */
  public boolean checkServerKeyProof(String proof) {
    return proof.equals(SRP.getServerKeyMatchProof(
        m_PublicEphemeral, m_ServerPublicEphemeral, m_SessionKey));
  }//checkServerKeyProof

  /**
   * Returns the raw session key.
   *
   * @return the session key as raw byte[].
   */
  public byte[] getSessionKey() {
    return m_SessionKey;
  }//getSessionKey

}//class ClientSRP

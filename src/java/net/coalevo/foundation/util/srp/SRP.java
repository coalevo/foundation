/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.srp;

import net.coalevo.foundation.util.crypto.DigestUtil;
import net.coalevo.foundation.util.crypto.RandomUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;


/**
 * Implements the base functionality required for authentication with
 * SRP Version 6a.
 * <p/>
 * Please see the
 * <a href="http://srp.stanford.edu/design.html">SRP Protocol Design Specification</a>
 * for more information about the protocol.
 * </p>
 * <p/>
 * For implementations the classes {@link ClientSRP}, {@link ServerSRP}
 * and {@link StoreEntry} should be used.
 * </p>
 * <p/>
 * Random bytes and digests are obtained through {@link RandomUtil} and
 * {@link DigestUtil}. Thus the defaults of these classes will have an
 * important effect on the outcome (e.g. lengths due to the hash etc.).
 * </p>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class SRP {

  /**
   * These values are taken from
   * http://www.ietf.org/internet-drafts/draft-ietf-tls-srp-10.txt
   * 2048 bit Group
   */
  private static final BigInteger c_Modulus = new BigInteger("ac6bdb41324a9a9bf166de5e1389582faf72b6651987ee07fc3192943db56050a37329cbb4a099ed8193e0757767a13dd52312ab4b03310dcd7f48a9da04fd50e8083969edb767b0cf6095179a163ab3661a05fbd5faaae82918a9962f0b93b855f97993ec975eeaa80d740adbf4ff747359d041d5c33ea71d281e446b14773bca97b43a23fb801676bd207a436c6481f1d2b9078717461a5b9d32e688f87748544523b524b0d57d5ea77a2775d2ecfa032cfbdbf52fb3786160279004e57ae6af874e7303ce53299ccc041c7bc308d82a5698f3a8d0c38271ae35f8e9dbfbb694b5c803d89f7ae435de236d525f54759b65e372fcd68ef20fa7111f9e4aff73", 16);
  private static final BigInteger c_Generator = new BigInteger("2", 10);
  private static BigInteger c_MultiplierParameter;
  private static byte[] c_ModulusGeneratorParam;
  private static final int SALT_SIZE = 12;
  public static final BigInteger c_Zero = new BigInteger("0", 10);
  public static final BigInteger c_One = new BigInteger("1", 10);


  static {
    if (Security.getProvider("CryptixCrypto") == null) {
      Provider p = new cryptix.jce.provider.CryptixCrypto();
      Security.addProvider(p);
    }

    MessageDigest md = DigestUtil.Pool.lease();
    try {
      //multiplier parameter
      md.update(c_Modulus.toByteArray());
      md.update(c_Generator.toByteArray());
      c_MultiplierParameter = new BigInteger(md.digest()).abs();
      //generator hash
      md.reset();
      md.update(c_Generator.toByteArray());
      BigInteger H_g = new BigInteger(md.digest()).abs();
      //modulus hash
      md.reset();
      md.update(c_Modulus.toByteArray());
      BigInteger H_N = new BigInteger(md.digest()).abs();


      c_ModulusGeneratorParam = H_N.xor(H_g).toByteArray();
      //System.out.println("H_N = " + H_N.toString(16));
      //System.out.println("ModGenParam = " + java.util.Arrays.toString(c_ModulusGeneratorParam));
      //System.out.println("Generator = " + c_Generator.toString(16));
      //System.out.println("ZEROS = " + c_Zero.toString(16));
      //System.out.println("ONES = " + c_One.toString(16));


    } finally {
      DigestUtil.Pool.release(md);
    }
  }//initializer

  private SRP() {

  }//private constructor

  /**
   * Splits a store entry of type <tt>$&lt;salt s&gt;$&lt;verifier v&gt;</tt>.
   * <p/>
   * Note that this method is for convenience, it does not
   * provide any checking of the salt and verifier values.
   * </p>
   *
   * @param str the entry.
   * @return a <tt>String[2]</tt> with the salt in the first slot
   *         and the verifier in the second.
   */
  public static String[] splitStoreEntry(String str) {
    String[] retval = new String[2];
    int idx = str.lastIndexOf("$");
    retval[0] = str.substring(str.indexOf("$") + 1, idx);
    retval[1] = str.substring(idx + 1, str.length());
    return retval;
  }//splitStoreEntry

  /**
   * Returns the SRP server store entry, with the format
   * <tt>$&lt;salt s&gt;$&lt;verifier v&gt;</tt>.
   * <p/>
   * The salt is a random generated <tt>byte[]<tt> of
   * length {@link #SALT_SIZE} converted to a hexadecimal string
   * representation.
   * </p>
   * <p/>
   * The verifier represents a <tt>BigInteger</tt> that is also
   * converted to a hexadecimal string representation.
   * It can be used simply by creating a big integer instance
   * directly from the encoded string <tt>new BigInteger(verifier,16)</tt>.
   * </p>
   * <p/>
   * The total length of the string representing the entry will depend
   * on the modulus used for the SRP modulus arithmetics.
   * This implementation (2048 bit group modulus and generator) will
   * likely produce strings around 537 bytes (average).
   * </p>
   *
   * @param p the password.
   * @return the server store entry.
   * @throws Exception if the creation of the entry fails.
   */
  public static String createStoreEntry(String p) throws Exception {

    MessageDigest md = DigestUtil.Pool.lease();
    try {
      byte[] pass = p.getBytes("UTF-8");
      byte[] salt = RandomUtil.getBlock(SALT_SIZE);
      md.update(salt);
      md.update(pass);
      byte[] x = md.digest();
      BigInteger v = c_Generator.modPow(new BigInteger(x).abs(), c_Modulus);
      StringBuilder sbuf = new StringBuilder();
      sbuf.append("$");
      sbuf.append(new String(Hex.encodeHex(salt)));
      sbuf.append("$");
      sbuf.append(v.toString(16));
      //System.err.println(sbuf.toString().length());
      return sbuf.toString();
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//createStoreEntry

  /**
   * Generates and returns the password verifier for
   * the given salt and the given password.
   *
   * @param s the salt bytes as hex encoded string.
   * @param p the password as UTF-8 stringprepped string.
   * @return the password verifier as hex encoded string representing
   *         a <tt>BigInteger</tt>.
   */
  public static String getPasswordVerifier(String s, String p) {
    MessageDigest md = DigestUtil.Pool.lease();
    try {
      byte[] pass = p.getBytes("UTF-8");
      byte[] salt = Hex.decodeHex(s.toCharArray());
      md.update(salt);
      md.update(pass);
      byte[] x = md.digest();
      BigInteger v = c_Generator.modPow(new BigInteger(x), c_Modulus);
      return v.toString(16);
    } catch (Exception ex) {
      return null;
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//getPasswordVerifier

  /**
   * Generates and returns the client ephemeral value A.
   * <p/>
   * A is obtained from the secret ephemeral value a applying:<br/>
   * <tt>A = g^a (modulus)</tt>.
   * </p>
   *
   * @param a the secret client ephemeral value as <tt>BigInteger</tt>.
   * @return the public ephemeral value as <tt>BigInteger</tt>.
   */
  public static BigInteger getClientPublicEphemeral(BigInteger a) {
    return c_Generator.modPow(a, c_Modulus);
  }//getClientPublicEphemeral

  /**
   * Returns the multiplier parameter k.
   * <p/>
   * This implementation is compliant with SRP-6a
   * where <tt>k = H(N, g)</tt>. The value is generated
   * only once at class initialization.
   * </p>
   *
   * @return the multiplier parameter.
   */
  public static BigInteger getMultiplierParameter() {
    return c_MultiplierParameter;
  }//getMultiplierParameter

  /**
   * Generates and returns the public server ephemeral value B.
   * <p/>
   * B is obtained from the secret ephemeral value b and the
   * password verifier v applying:<br/>
   * <tt>B = kv + g^b</tt>
   * </p>
   *
   * @param v the password verifier as <tt>BigInteger</tt>.
   * @param b the server secret ephemeral value as <tt>BigInteger</tt>.
   * @return the public emphemeral value as <tt>BigInteger</tt>.
   */
  public static BigInteger getServerPublicEphemeral(BigInteger v, BigInteger b) {
    return c_MultiplierParameter.multiply(v).add(c_Generator.modPow(b, c_Modulus));
  }//getServerPublicEphemeral

  /**
   * Generates and returns the random scrambling parameter u.
   * <p/>
   * where <tt>u= H(A, B)</tt>
   * </p>
   *
   * @param A the client public ephemeral value.
   * @param B the server public ephemeral value.
   * @return the random scrambling parameter u as <tt>BigInteger</tt>.
   */
  public static BigInteger getRandomScramblingParameter(BigInteger A, BigInteger B) {
    MessageDigest md = DigestUtil.Pool.lease();
    try {
      md.update(A.toByteArray());
      md.update(B.toByteArray());
      return new BigInteger(md.digest()).abs();
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//getRandomScramblingParameter

  /**
   * Generates and returns the strong session key K
   * at client side.
   * <p/>
   * where:
   * <pre>
   * S = (B - kg^x) ^ (a + ux)   (computes session key)
   * K = H(S)
   * </pre>
   * </p>
   *
   * @param salt the random salt bytes as hex encoded string.
   * @param pass the users password.
   * @param B    the server's public ephemeral value.
   * @param a    the clients secret ephemeral value.
   * @param u    the random scrambling parameter.
   * @return the strong session key K as <tt>byte[]</tt> where the length
   *         depends on the digest applied per default by the {@link DigestUtil}.
   * @throws Exception if the password string is not UTF-8 compatible.
   */
  public static byte[] getClientSessionKey(String salt, String pass, BigInteger B, BigInteger a, BigInteger u) throws Exception {
    MessageDigest md = DigestUtil.Pool.lease();
    BigInteger x = null;
    //calculate x
    try {
      md.update(Hex.decodeHex(salt.toCharArray()));
      md.update(pass.getBytes("UTF-8"));
      x = new BigInteger(md.digest()).abs();
    } finally {
      DigestUtil.Pool.release(md);
    }
    BigInteger S =
        B.subtract(c_MultiplierParameter.multiply(c_Generator.modPow(x, c_Modulus)))
            .modPow(a.add(u.multiply(x)), c_Modulus);
    md = DigestUtil.Pool.lease();
    try {
      return md.digest(S.toByteArray());
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//getSessionKey

  /**
   * Generates and returns the strong session key K
   * at server side.
   * <p/>
   * where:
   * <pre>
   * S = (Av^u) ^ b   (computes session key)
   * K = H(S)
   * </pre>
   * </p>
   *
   * @param v the password verifier.
   * @param A the clients public ephemeral value.
   * @param b the servers secret ephemeral value.
   * @param u the random scrambling parameter.
   * @return the strong session key K as <tt>byte[]</tt> where the length
   *         depends on the digest applied per default by the {@link DigestUtil}.
   */
  public static byte[] getServerSessionKey(BigInteger v, BigInteger A, BigInteger b, BigInteger u) {
    BigInteger S = v.modPow(u, c_Modulus).multiply(A).modPow(b, c_Modulus);
    MessageDigest md = DigestUtil.Pool.lease();
    try {
      return md.digest(S.toByteArray());
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//getServerSessionKey

  /**
   * Returns the client's key match proof M.
   * <p/>
   * This match proof is calculated as suggested in the specifications:
   * <pre>
   * M = H(H(N) xor H(g), H(I), s, A, B, K)
   * </pre>
   * </p>
   * <p/>
   * Note that in this implementation <tt>H(H(N) xor H(g)</tt> is
   * prepared only once at class initialization.
   * </p>
   *
   * @param id   the client identifier (e.g. username).
   * @param salt the corresponding salt bytes as hex encoded string.
   * @param A    the clients public ephemeral value.
   * @param B    the servers public ephemeral value.
   * @param K    the strong session key K.
   * @return the client match proof M as Base64 encoded string.
   */
  public static String getClientKeyMatchProof(String id, String salt, BigInteger A, BigInteger B, byte[] K) {

    // User -> Host:  M = H(H(N) xor H(g), H(I), s, A, B, K)
    MessageDigest md = DigestUtil.Pool.lease();
    //calculate x
    try {
      md.update(c_ModulusGeneratorParam);
      try {
        md.update(DigestUtil.digest(id.getBytes("UTF-8")));
        md.update(Hex.decodeHex(salt.toCharArray()));
      } catch (Exception ex) {
        //should not happen, match will fail.
      }
      md.update(A.toByteArray());
      md.update(B.toByteArray());
      md.update(K);
      return new String(Base64.encodeBase64(md.digest()));
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//getClientMatchProof

  /**
   * Returns the server's key match proof Ms.
   * <p/>
   * This match proof is calculated as suggested in the specifications:
   * <pre>
   * Ms= H(A, M, K)
   * </pre>
   * </p>
   *
   * @param A the client's public ephemeral value.
   * @param B the server's public ephemeral value.
   * @param K the strong session key.
   * @return the server's match proof M as Base64 encoded string.
   */
  public static String getServerKeyMatchProof(BigInteger A, BigInteger B, byte[] K) {
    //Host -> User:  H(A, M, K)
    MessageDigest md = DigestUtil.Pool.lease();
    try {
      md.update(A.toByteArray());
      md.update(B.toByteArray());
      md.update(K);
      return new String(Base64.encodeBase64(md.digest()));
    } finally {
      DigestUtil.Pool.release(md);
    }
  }//getServerMatchProof

  /**
   * Generates and returns random salt bytes as hex encoded
   * string.
   *
   * @return random bytes as hex encoded string.
   * @see {@link#SALT_SIZE}.
   */
  public static String getSalt() {
    return new String(Hex.encodeHex(RandomUtil.getBlock(SALT_SIZE)));
  }//getSalt

  /**
   * Generates and returns a random <tt>BigInteger</tt> of
   * a given length.
   *
   * @param len the length of random bytes to be used.
   * @return a random <tt>BigInteger</tt>.
   */
  public static BigInteger getRandomBigInteger(int len) {
    BigInteger bi = new BigInteger(RandomUtil.getBlock(len));
    if (bi.compareTo(c_Modulus) >= 0) {
      bi = bi.mod(c_Modulus.subtract(c_One));
    }
    return bi.abs();
  }//getRandomBigInteger

  //Test Routine
  public static void main(String[] args) throws Exception {
    //Password entry
    String username = "user";
    String password = "password";
    String storeEntry = createStoreEntry(password);
    String[] split = splitStoreEntry(storeEntry);

    String salt = split[0];
    BigInteger v = new BigInteger(split[1], 16);
    System.out.println(v.signum());

    //User:
    BigInteger a = getRandomBigInteger(32);
    BigInteger A = getClientPublicEphemeral(a);
    System.out.println(a.signum());
    System.out.println(A.signum());

    System.out.print("User -> ");
    System.out.print(username);
    System.out.print(", A = ");
    System.out.println(A.toString(16));

    //Host:
    BigInteger b = getRandomBigInteger(32);
    BigInteger B = getServerPublicEphemeral(v, b);
    System.out.println(b.signum());
    System.out.println(B.signum());

    System.out.print("Host -> ");
    System.out.print(salt);
    System.out.print(" , B = ");
    System.out.println(B.toString(16));

    //Both:  u = H(A, B)
    BigInteger u = SRP.getRandomScramblingParameter(A, B);
    System.out.println(u.signum());

    //User:
    byte[] K = SRP.getClientSessionKey(salt, password, B, a, u);
    System.out.println("User K= " + new String(Hex.encodeHex(K)));
    //Host:
    System.out.println("Host K= " + new String(Hex.encodeHex(SRP.getServerSessionKey(v, A, b, u))));

    System.out.println("User Match Proof = " + SRP.getClientKeyMatchProof(username, salt, A, B, K));
    System.out.println("Host Match Proof = " + SRP.getServerKeyMatchProof(A, B, K));

    //prepare KDF
    net.coalevo.foundation.util.crypto.KDF kdf = new net.coalevo.foundation.util.crypto.KDF();
    kdf.init(K, (byte)21);
    byte[] cryptkey = kdf.nextKey();

    net.coalevo.foundation.util.crypto.BlockCipherFactory cf = new net.coalevo.foundation.util.crypto.BlockCipherFactory();
    byte[] iv = RandomUtil.getBlock(16);
    javax.crypto.Cipher cipher = cf.createEncryptionCypher(cryptkey,iv);

    byte[] ct ="This is just an example looooooooooooooooooooooonger!".getBytes();
    java.nio.ByteBuffer inbuf = java.nio.ByteBuffer.allocate(ct.length);
    inbuf.put(ct);
    inbuf.flip();
    System.out.println("Buffer = " + new String(inbuf.array()));
    System.out.println("Array = " + new String(ct));

    java.nio.ByteBuffer outbuf = java.nio.ByteBuffer.allocate(cipher.getOutputSize(ct.length));

    cipher.doFinal(inbuf,outbuf);
    outbuf.flip();
    cipher = cf.createEncryptionCypher(cryptkey,iv);

    byte[] encrypted = cipher.doFinal(ct);

    System.out.println("Buf encrypted string: " + new String(Hex.encodeHex(outbuf.array())));
    System.out.println("Arr encrypted string: " + new String(Hex.encodeHex(encrypted)));


    cipher = cf.createDecryptionCypher(cryptkey,iv);
    byte[] decrypted =
        cipher.doFinal(encrypted);
    cipher = cf.createDecryptionCypher(cryptkey,iv);
    java.nio.ByteBuffer dec = java.nio.ByteBuffer.allocate(cipher.getOutputSize(outbuf.remaining()));
    cipher.doFinal(outbuf,dec);
    dec.flip();

    System.out.println("Buf decrypted string: " + new String(dec.array()));
    System.out.println("Arr decrypted string: " + new String(decrypted));

    testGen();
  }//main


  /* Test routines for the entry generator */
  public static void testGen() {
    long len = 0;
    long totaltime = 0;
    long start = 0;
    try {
      for (int i = 0; i< 100; i++) {
        start=System.currentTimeMillis();
        len += createStoreEntry(new String(Hex.encodeHex(RandomUtil.getBlock(8)))).length();
        totaltime += System.currentTimeMillis()-start;
      }
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
    }
    System.out.println("Length Avg.=" + (len/100));
    System.out.println("Total generation time = " + totaltime + " [ms].");
    System.out.println("Average generation time = " + (totaltime/100) + " [ms].");

  }


}//class SRP

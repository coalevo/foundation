/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.srp;

import java.math.BigInteger;

/**
 * Provides an SRP helper for SRP server side.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class ServerSRP {

  private String m_Username;
  private String m_Salt;
  private BigInteger m_Verifier;

  private BigInteger m_SecretEphemeral; //b
  private BigInteger m_PublicEphemeral; //B
  private BigInteger m_ClientPublicEphemeral; //A

  private byte[] m_SessionKey;
  private String m_KeyMatchProof;

  private boolean m_Authenticated = false;

  public ServerSRP(StoreEntry se) {
    m_Username = se.getUsername();
    m_Salt = se.getSalt();
    m_Verifier = new BigInteger(se.getPasswordVerifier(), 16);
    //init
    m_SecretEphemeral = SRP.getRandomBigInteger(32);
    m_PublicEphemeral = SRP.getServerPublicEphemeral(m_Verifier, m_SecretEphemeral);
  }//ServerSRP

  /**
   * Returns the salt stored for the identity.
   *
   * @return the salt as HEX encoded String.
   */
  public String getSalt() {
    return m_Salt;
  }//getSalt

  /**
   * Returns the public client ephemeral value.
   *
   * @return a String of hex encoded bytes representing a BigInteger value.
   */
  public String getPublicEphemeral() {
    return m_PublicEphemeral.toString(16);
  }//getPublicEphemeral

  /**
   * Should be called when the client returned the session init.
   *
   * @param Ahex the client public ephemeral as hex encoded BigInteger value.
   * @return false if aborted due to safeguards or key generation errors.
   */
  public boolean received(String Ahex) {
    m_ClientPublicEphemeral = new BigInteger(Ahex, 16);
    if (m_ClientPublicEphemeral.equals(SRP.c_Zero)) {
      return false;
    }
    //calculate random scrambling

    BigInteger u = SRP.getRandomScramblingParameter(m_ClientPublicEphemeral, m_PublicEphemeral);
    if (u.equals(SRP.c_Zero)) {
      return false;
    }
    //calulate key
    try {
      m_SessionKey = SRP.getServerSessionKey(m_Verifier, m_ClientPublicEphemeral, m_SecretEphemeral, u);
    } catch (Exception ex) {
      return false;
    }
    m_KeyMatchProof = SRP.getServerKeyMatchProof(m_ClientPublicEphemeral, m_PublicEphemeral, m_SessionKey);
    return true;
  }//received

  /**
   * Return the client key match proof.
   *
   * @return a client key match proof.
   */
  public String getKeyMatchProof() {
    return m_KeyMatchProof;
  }//getKeyMatchProof

  /**
   * Returns the raw session key.
   *
   * @return the session key as raw byte[].
   */
  public byte[] getSessionKey() {
    return m_SessionKey;
  }//getSessionKey

  /**
   * Tests the client key proof.
   *
   * @param proof the proof received from the client.
   * @return true if ok, false otherwise.
   */
  public boolean checkClientKeyProof(String proof) {
    m_Authenticated = proof.equals(SRP.getClientKeyMatchProof(
        m_Username, m_Salt, m_ClientPublicEphemeral, m_PublicEphemeral, m_SessionKey));
    return m_Authenticated;
  }//checkServerKeyProof

  /**
   * Tests if this <tt>ServerSRP</tt> is authenticated.
   * <p/>
   * Authentication is set when the client match proof check was
   * called successfully.
   * </p>
   *
   * @return true if authenticated, false otherwise.
   * @see #checkClientKeyProof(String)
   */
  public boolean isAuthenticated() {
    return m_Authenticated;
  }//isAuthenticated

  /**
   * Returns the associated username.
   *
   * @return the associated username.
   */
  public String getUsername() {
    return m_Username;
  }//getUsername

}//class ServerSRP

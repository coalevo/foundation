/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.foundation.util.srp;

import net.coalevo.foundation.util.crypto.RandomUtil;
import org.apache.commons.codec.binary.Hex;

/**
 * Provides a store entry for SRP.
 * <p/>
 * This entries are composed of $&lt;salt&gt;$&lt;password verifier&gt;
 * and have a length of approx. 100 bytes.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class StoreEntry {

  private String m_Username;
  private String m_Salt;
  private String m_PasswordVerifier;
  private String m_Entry;

  public StoreEntry(String username, String entry) {
    setEntry(entry);
    m_Username = username;
  }//StoreEntry

  public String getUsername() {
    return m_Username;
  }//getUsername

  public void setUsername(String username) {
    m_Username = username;
  }//setUsername

  public String getSalt() {
    return m_Salt;
  }//getSalt

  public void setSalt(String salt) {
    m_Salt = salt;
  }//setSalt

  public String getPasswordVerifier() {
    return m_PasswordVerifier;
  }//getPasswordVerifier

  public void setPasswordVerifier(String passwordVerifier) {
    m_PasswordVerifier = passwordVerifier;
  }//setPasswordVerifier

  public boolean checkPassword(String p) {
    if (p != null && m_Salt != null && m_PasswordVerifier != null) {
      String v = SRP.getPasswordVerifier(m_Salt, p);
      return m_PasswordVerifier.equals(v);
    }
    return false;
  }//checkPasswordVerifier

  public String toString() {
    return m_Entry;
  }//toString

  private void setEntry(String str) {
    int idx = str.lastIndexOf("$");
    m_Salt = str.substring(str.indexOf("$") + 1, idx);
    m_PasswordVerifier = str.substring(idx + 1, str.length());
    m_Entry = str;
  }//setEntry

  public static final StoreEntry createStoreEntry(String username, String password)
      throws Exception {
    return new StoreEntry(username, SRP.createStoreEntry(password));
  }//createStoreEntry

  public static final StoreEntry createFakeEntry(String username)
      throws Exception {
    return new StoreEntry(username, SRP.createStoreEntry(
        new String(Hex.encodeHex(RandomUtil.getBlock(8)))
    ));
  }//createStoreEntry

  public static final String createFakeSalt() {
    return SRP.getSalt();
  }//createFakeSalt

}//class StoreEntry
